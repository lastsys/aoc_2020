use common::input::fold_input;
use common::performance::timing;
use std::io::Error;

#[derive(Debug, PartialEq, Clone)]
enum Token {
    Add,
    Sub,
    Mul,
    Number(isize),
    Begin,
    End,
}

fn tokenize(input: &str) -> Vec<Token> {
    let mut tokens: Vec<Token> = Vec::new();
    let mut term: Vec<char> = Vec::new();
    for character in input.chars() {
        if !term.is_empty() {
            let number: String = term.into_iter().collect();
            tokens.push(Token::Number(
                number.parse::<isize>().expect("Could not parse number."),
            ));
            term = Vec::new();
        }
        match character {
            c if c.is_numeric() => {
                term.push(character);
            }
            '+' => {
                tokens.push(Token::Add);
            }
            '-' => {
                tokens.push(Token::Sub);
            }
            '*' => {
                tokens.push(Token::Mul);
            }
            '(' => {
                tokens.push(Token::Begin);
            }
            ')' => {
                tokens.push(Token::End);
            }
            _ => (),
        };
    }
    if !term.is_empty() {
        let number: String = term.into_iter().collect();
        tokens.push(Token::Number(
            number.parse::<isize>().expect("Could not parse number."),
        ));
    }
    tokens
}

fn apply(operator: &Token, lhs: isize, rhs: isize) -> isize {
    match operator {
        Token::Add => lhs + rhs,
        Token::Sub => lhs - rhs,
        Token::Mul => lhs * rhs,
        _ => panic!("Undefined operator."),
    }
}

fn eval(tokens: &[Token]) -> (isize, &[Token]) {
    let mut rest: &[Token] = &tokens[0..];
    let mut total: isize = 0;
    let mut operator = Token::Add;
    while !rest.is_empty() {
        let token = &rest[0];
        rest = &rest[1..];
        match token {
            Token::Add => {
                operator = Token::Add;
            }
            Token::Sub => {
                operator = Token::Sub;
            }
            Token::Mul => {
                operator = Token::Mul;
            }
            Token::Begin => {
                let (term_total, new_rest) = eval(rest);
                total = apply(&operator, total, term_total);
                rest = new_rest;
            }
            Token::End => {
                return (total, rest);
            }
            Token::Number(number) => {
                total = apply(&operator, total, *number);
            }
        }
    }
    (total, rest)
}

fn input_folder(container: &mut Vec<String>, line: Result<String, Error>) -> &mut Vec<String> {
    let expr = line.expect("Could not read line.").trim().to_string();
    container.push(expr);
    container
}

fn part1() {
    let mut init: Vec<String> = Vec::new();
    let expressions =
        fold_input("input/day18.txt", &mut init, input_folder).expect("Could not read file.");
    let sum: isize = expressions
        .iter()
        .map(|expr| {
            let tokens = tokenize(expr);
            eval(&tokens).0
        })
        .sum();
    println!("{}", sum);
}

// Using FORTRAN I approach. https://en.wikipedia.org/wiki/Operator-precedence_parser
fn precedence(tokens: &[Token]) -> Vec<Token> {
    let mut output: Vec<Token> = Vec::new();

    output.extend(vec![Token::Begin, Token::Begin]);

    for token in tokens {
        match token {
            Token::Add => {
                output.extend(vec![Token::End, Token::Add, Token::Begin]);
            }
            Token::Sub => {
                output.extend(vec![Token::End, Token::Sub, Token::Begin]);
            }
            Token::Mul => output.extend(vec![
                Token::End,
                Token::End,
                Token::Mul,
                Token::Begin,
                Token::Begin,
            ]),
            Token::Begin => output.extend(vec![Token::Begin, Token::Begin, Token::Begin]),
            Token::End => output.extend(vec![Token::End, Token::End, Token::End]),
            t => {
                output.push(t.clone());
            }
        };
    }

    output.extend(vec![Token::End, Token::End]);

    output
}

fn part2() {
    let mut init: Vec<String> = Vec::new();
    let expressions =
        fold_input("input/day18.txt", &mut init, input_folder).expect("Could not read file.");
    let sum: isize = expressions
        .iter()
        .map(|expr| {
            let mut tokens = tokenize(expr);
            tokens = precedence(&tokens);
            eval(&tokens).0
        })
        .sum();
    println!("{}", sum);
}

fn main() {
    timing(part1, 18, 1);
    timing(part2, 18, 2);
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_example_part_1_ex_1() {
        let input = "1 + 2 * 3 + 4 * 5 + 6";
        let tokens = tokenize(input);
        assert_eq!(
            tokens,
            vec![
                Token::Number(1),
                Token::Add,
                Token::Number(2),
                Token::Mul,
                Token::Number(3),
                Token::Add,
                Token::Number(4),
                Token::Mul,
                Token::Number(5),
                Token::Add,
                Token::Number(6)
            ]
        );
        let result = eval(&tokens);
        assert_eq!(result.0, 71);
    }

    #[test]
    fn test_part_1_ex_2() {
        let input = "1 + (2 * 3) + (4 * (5 + 6))";
        let tokens = tokenize(input);
        assert_eq!(
            tokens,
            vec![
                Token::Number(1),
                Token::Add,
                Token::Begin,
                Token::Number(2),
                Token::Mul,
                Token::Number(3),
                Token::End,
                Token::Add,
                Token::Begin,
                Token::Number(4),
                Token::Mul,
                Token::Begin,
                Token::Number(5),
                Token::Add,
                Token::Number(6),
                Token::End,
                Token::End
            ]
        );
        let result = eval(&tokens);
        assert_eq!(result.0, 51);
    }

    #[test]
    fn test_part_1_ex_3() {
        let input = "2 * 3 + (4 * 5)";
        let tokens = tokenize(input);
        let result = eval(&tokens);
        assert_eq!(result.0, 26);
    }

    #[test]
    fn test_part_1_ex_4() {
        let input = "5 + (8 * 3 + 9 + 3 * 4 * 3)";
        let tokens = tokenize(input);
        let result = eval(&tokens);
        assert_eq!(result.0, 437);
    }

    #[test]
    fn test_part_1_ex_5() {
        let input = "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))";
        let tokens = tokenize(input);
        let result = eval(&tokens);
        assert_eq!(result.0, 12240);
    }

    #[test]
    fn test_part_1_ex_6() {
        let input = "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2";
        let tokens = tokenize(input);
        let result = eval(&tokens);
        assert_eq!(result.0, 13632);
    }

    #[test]
    fn test_part_2_ex_1() {
        let input = "1 + 2 * 3 + 4 * 5 + 6";
        let mut tokens = tokenize(input);
        tokens = precedence(&tokens);
        let result = eval(&tokens);
        assert_eq!(result.0, 231);
    }

    #[test]
    fn test_part_2_ex_2() {
        let input = "1 + (2 * 3) + (4 * (5 + 6))";
        let mut tokens = tokenize(input);
        tokens = precedence(&tokens);
        let result = eval(&tokens);
        assert_eq!(result.0, 51);
    }

    #[test]
    fn test_part_2_ex_3() {
        let input = "2 * 3 + (4 * 5)";
        let mut tokens = tokenize(input);
        tokens = precedence(&tokens);
        let result = eval(&tokens);
        assert_eq!(result.0, 46);
    }

    #[test]
    fn test_part_2_ex_4() {
        let input = "5 + (8 * 3 + 9 + 3 * 4 * 3)";
        let mut tokens = tokenize(input);
        tokens = precedence(&tokens);
        let result = eval(&tokens);
        assert_eq!(result.0, 1445);
    }

    #[test]
    fn test_part_2_ex_5() {
        let input = "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))";
        let mut tokens = tokenize(input);
        tokens = precedence(&tokens);
        let result = eval(&tokens);
        assert_eq!(result.0, 669060);
    }

    #[test]
    fn test_part_2_ex_6() {
        let input = "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2";
        let mut tokens = tokenize(input);
        tokens = precedence(&tokens);
        let result = eval(&tokens);
        assert_eq!(result.0, 23340);
    }
}
