use common::input::fold_input;
use common::performance::timing;
use std::io::Error;

/// Input file folder converting each row to an integer.
fn input_folder(container: &mut Vec<u32>, line: Result<String, Error>) -> &mut Vec<u32> {
    container.push(line.unwrap().parse().unwrap());
    container
}

/// Find a sum of pairs in the list of numbers and return them.
fn find_sum<A>(numbers: &[A], sum: A) -> Option<(A, A)>
where
    A: num::Integer + Copy,
{
    // We only need to test half of the matrix excluding the diagonal.
    for i in 0..numbers.len() {
        for j in (i + 1)..numbers.len() {
            if (numbers[i] + numbers[j]) == sum {
                return Some((numbers[i], numbers[j]));
            }
        }
    }
    None
}

/// Find a sum of triplets in the list of numbers and return them.
fn find_sum_3<A>(numbers: &[A], sum: A) -> Option<(A, A, A)>
where
    A: num::Integer + Copy,
{
    // We have a diagonally sliced cube.
    for i in 0..numbers.len() {
        for j in (i + 1)..numbers.len() {
            for k in (j + 1)..numbers.len() {
                if (numbers[i] + numbers[j] + numbers[k]) == sum {
                    return Some((numbers[i], numbers[j], numbers[k]));
                }
            }
        }
    }
    None
}

fn part1() {
    let mut init: Vec<u32> = Vec::new();
    let numbers = fold_input("input/day01.txt", &mut init, input_folder).unwrap();

    match find_sum(&numbers, 2020) {
        Some((ni, nj)) => {
            println!("{} + {} = 2020", ni, nj);
            println!("{} * {} = {}", ni, nj, ni * nj);
        }
        None => panic!("Could not find numbers."),
    }
}

fn part2() {
    let mut init: Vec<u32> = Vec::new();
    let numbers = fold_input("input/day01.txt", &mut init, input_folder).unwrap();

    match find_sum_3(&numbers, 2020) {
        Some((ni, nj, nk)) => {
            println!("{} + {} + {} = 2020", ni, nj, nk);
            println!("{} * {} * {} = {}", ni, nj, nk, ni * nj * nk);
        }
        None => panic!("Could not find numbers."),
    }
}

fn main() {
    timing(part1, 1, 1);
    timing(part2, 1, 2);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_sum() {
        let numbers: Vec<u32> = vec![1721, 979, 366, 299, 675, 1456];
        match find_sum(&numbers, 2020) {
            Some((ni, nj)) => {
                assert_eq!(ni, 1721);
                assert_eq!(nj, 299);
            }
            None => panic!("Could not find sum."),
        };
    }

    #[test]
    fn test_find_sum_3() {
        let numbers: Vec<u32> = vec![1721, 979, 366, 299, 675, 1456];
        match find_sum_3(&numbers, 2020) {
            Some((ni, nj, nk)) => {
                assert_eq!(ni, 979);
                assert_eq!(nj, 366);
                assert_eq!(nk, 675);
            }
            None => panic!("Could not find sum of three."),
        };
    }
}
