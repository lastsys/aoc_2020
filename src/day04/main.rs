use common::input::fold_input;
use common::performance::timing;
use regex::Regex;
use std::collections::HashMap;
use std::io::Error;

type Passport = HashMap<String, String>;

fn input_folder(container: &mut Vec<String>, line: Result<String, Error>) -> &mut Vec<String> {
    container.push(line.expect("Could not read line.").trim().to_string());
    container
}

fn parse_passports(input: &[String]) -> Vec<Passport> {
    // Split on empty rows.
    let sections: Vec<String> = input.split(|s| s.is_empty()).map(|x| x.join(" ")).collect();

    let mut passports: Vec<Passport> = Vec::new();
    for section in sections {
        let mut passport = Passport::new();
        let fields: Vec<&str> = section.split_whitespace().collect();
        for field in fields {
            let parts: Vec<&str> = field.split(':').collect();
            passport.insert(parts[0].to_string(), parts[1].to_string());
        }
        passports.push(passport);
    }
    passports
}

fn passport_is_valid(passport: &Passport) -> bool {
    passport.len() == 8 || (passport.len() == 7 && !passport.contains_key("cid"))
}

fn validate_birth_year(year: &str) -> bool {
    matches!(year.parse::<usize>(), Ok(n) if (n >= 1920) && (n <= 2002))
}

fn validate_issue_year(year: &str) -> bool {
    matches!(year.parse::<usize>(), Ok(n) if (n >= 2010) && (n <= 2020))
}

fn validate_expiration_year(year: &str) -> bool {
    matches!(year.parse::<usize>(), Ok(n) if (n >= 2020) && (n <= 2030))
}

fn validate_height(height: &str) -> bool {
    let re = Regex::new(r"(\d+)(in|cm)").expect("Failed to generate regular expression.");
    match re.captures(height) {
        Some(c) if c.len() == 3 => match (c[1].parse::<usize>(), &c[2]) {
            (Ok(n), "cm") if (n >= 150) && (n <= 193) => true,
            (Ok(n), "in") if (n >= 59) && (n <= 76) => true,
            _ => false,
        },
        _ => false,
    }
}

fn validate_hair_color(color: &str) -> bool {
    let re = Regex::new(r"#([0-9]|[a-f]){6}").expect("Failed to generate regular expression.");
    re.is_match(color)
}

fn validate_eye_color(color: &str) -> bool {
    matches!(color, "amb" | "blu" | "brn" | "gry" | "grn" | "hzl" | "oth")
}

fn validate_passport_id(id: &str) -> bool {
    id.len() == 9 && id.parse::<usize>().is_ok()
}

fn passport_is_valid_2(passport: &Passport) -> bool {
    passport_is_valid(passport)
        && validate_birth_year(&passport["byr"])
        && validate_issue_year(&passport["iyr"])
        && validate_expiration_year(&passport["eyr"])
        && validate_height(&passport["hgt"])
        && validate_hair_color(&passport["hcl"])
        && validate_eye_color(&passport["ecl"])
        && validate_passport_id(&passport["pid"])
}

fn part1() {
    let mut init: Vec<String> = Vec::new();
    let lines =
        fold_input("input/day04.txt", &mut init, input_folder).expect("Could not read file.");
    let passports = parse_passports(lines);
    let valid_passports: Vec<&Passport> =
        passports.iter().filter(|p| passport_is_valid(p)).collect();
    println!("Total passport count = {}", passports.len());
    println!("Valid passports = {}", valid_passports.len());
}

fn part2() {
    let mut init: Vec<String> = Vec::new();
    let lines =
        fold_input("input/day04.txt", &mut init, input_folder).expect("Could not read file.");
    let passports = parse_passports(lines);
    let valid_passports: Vec<&Passport> = passports
        .iter()
        .filter(|p| passport_is_valid_2(p))
        .collect();
    println!("Valid passports = {}", valid_passports.len());
}

fn main() {
    timing(part1, 4, 1);
    timing(part2, 4, 2);
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_DATA: &[&str] = &[
        "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd",
        "byr:1937 iyr:2017 cid:147 hgt:183cm",
        "",
        "iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884",
        "hcl:#cfa07d byr:1929",
        "",
        "hcl:#ae17e1 iyr:2013",
        "eyr:2024",
        "ecl:brn pid:760753108 byr:1931",
        "hgt:179cm",
        "",
        "hcl:#cfa07d eyr:2025 pid:166559648",
        "iyr:2011 ecl:brn hgt:59in",
    ];

    #[test]
    fn test_parse_passports() {
        let test_data: Vec<String> = TEST_DATA.iter().map(|s| s.to_string()).collect();
        let passports = parse_passports(&test_data);
        assert_eq!(passports.len(), 4);
        assert_eq!(passports[0].len(), 8);
        assert_eq!(passports[1].len(), 7);
        assert_eq!(passports[2].len(), 7);
        assert_eq!(passports[3].len(), 6);

        let valid_passports: Vec<&Passport> =
            passports.iter().filter(|p| passport_is_valid(p)).collect();
        assert_eq!(valid_passports.len(), 2);
    }

    #[test]
    fn test_validators() {
        assert!(validate_birth_year(&String::from("2002")));
        assert!(!validate_birth_year(&String::from("2003")));

        assert!(validate_height(&String::from("60in")));
        assert!(validate_height(&String::from("190cm")));
        assert!(!validate_height(&String::from("190in")));
        assert!(!validate_height(&String::from("190")));

        assert!(validate_hair_color(&String::from("#123abc")));
        assert!(!validate_hair_color(&String::from("#123abz")));
        assert!(!validate_hair_color(&String::from("123abc")));

        assert!(validate_eye_color(&String::from("brn")));
        assert!(!validate_eye_color(&String::from("wat")));

        assert!(validate_passport_id(&String::from("000000001")));
        assert!(!validate_passport_id(&String::from("0123456789")));
    }

    const INVALID_DATA: &[&str] = &[
        "eyr:1972 cid:100",
        "hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926",
        "",
        "iyr:2019",
        "hcl:#602927 eyr:1967 hgt:170cm",
        "ecl:grn pid:012533040 byr:1946",
        "",
        "hcl:dab227 iyr:2012",
        "ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277",
        "",
        "hgt:59cm ecl:zzz",
        "eyr:2038 hcl:74454a iyr:2023",
        "pid:3556412378 byr:2007",
    ];

    #[test]
    fn test_invalid_passports() {
        let test_data: Vec<String> = INVALID_DATA.iter().map(|s| s.to_string()).collect();
        let passports = parse_passports(&test_data);
        assert_eq!(passports.len(), 4);
        let invalid_passports: Vec<&Passport> = passports
            .iter()
            .filter(|p| !passport_is_valid_2(p))
            .collect();
        assert_eq!(invalid_passports.len(), 4);
    }

    const VALID_DATA: &[&str] = &[
        "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980",
        "hcl:#623a2f",
        "",
        "eyr:2029 ecl:blu cid:129 byr:1989",
        "iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm",
        "",
        "hcl:#888785",
        "hgt:164cm byr:2001 iyr:2015 cid:88",
        "pid:545766238 ecl:hzl",
        "eyr:2022",
        "",
        "iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719",
    ];

    #[test]
    fn test_valid_passports() {
        let test_data: Vec<String> = VALID_DATA.iter().map(|s| s.to_string()).collect();
        let passports = parse_passports(&test_data);
        assert_eq!(passports.len(), 4);
        let valid_passports: Vec<&Passport> = passports
            .iter()
            .filter(|p| passport_is_valid_2(p))
            .collect();
        assert_eq!(valid_passports.len(), 4);
    }
}
