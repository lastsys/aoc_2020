use common::performance::timing;
use std::collections::{HashSet, VecDeque};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::iter::FromIterator;

enum Winner {
    Player1,
    Player2,
}

struct Game {
    player_1: VecDeque<usize>,
    player_2: VecDeque<usize>,
}

impl Game {
    pub fn step(&mut self) -> bool {
        let p1 = self
            .player_1
            .pop_front()
            .expect("Could not get top card p1.");
        let p2 = self
            .player_2
            .pop_front()
            .expect("Could not get top card p2.");
        if p1 > p2 {
            self.player_1.push_back(p1);
            self.player_1.push_back(p2);
        } else {
            self.player_2.push_back(p2);
            self.player_2.push_back(p1);
        }
        self.player_1.is_empty() || self.player_2.is_empty()
    }

    pub fn score(&self) -> usize {
        if self.player_2.is_empty() {
            self.player_1
                .iter()
                .rev()
                .enumerate()
                .map(|(i, v)| (i + 1) * v)
                .sum()
        } else {
            self.player_2
                .iter()
                .rev()
                .enumerate()
                .map(|(i, v)| (i + 1) * v)
                .sum()
        }
    }
}

fn recursive(
    mut deck_p1: VecDeque<usize>,
    mut deck_p2: VecDeque<usize>,
) -> (Winner, VecDeque<usize>) {
    let mut played_set: HashSet<(VecDeque<usize>, VecDeque<usize>)> = HashSet::new();
    while !(deck_p1.is_empty() || deck_p2.is_empty()) {
        if !played_set.insert((deck_p1.clone(), deck_p2.clone())) {
            return (Winner::Player1, deck_p1);
        }

        let p1_card = deck_p1.pop_front().expect("Could not get top card 1.");
        let p2_card = deck_p2.pop_front().expect("Could not get top card 2.");

        let winner = if deck_p1.len() >= p1_card as usize && deck_p2.len() >= p2_card as usize {
            let new_deck_p1 = deck_p1.iter().take(p1_card as usize).cloned().collect();
            let new_deck_p2 = deck_p2.iter().take(p2_card as usize).cloned().collect();
            recursive(new_deck_p1, new_deck_p2).0
        } else if p1_card > p2_card {
            Winner::Player1
        } else if p2_card > p1_card {
            Winner::Player2
        } else {
            continue;
        };

        match winner {
            Winner::Player1 => {
                deck_p1.push_back(p1_card);
                deck_p1.push_back(p2_card);
            }
            Winner::Player2 => {
                deck_p2.push_back(p2_card);
                deck_p2.push_back(p1_card);
            }
        };
    }

    if deck_p1.is_empty() {
        (Winner::Player2, deck_p2)
    } else {
        (Winner::Player1, deck_p1)
    }
}

fn parse_lines(lines: &[String]) -> Game {
    let parts: Vec<_> = lines.split(|s| s.is_empty()).collect();

    fn parse_player(player: &[String]) -> Vec<usize> {
        player
            .iter()
            .skip(1)
            .map(|s| s.parse::<usize>().expect("Could not parse number."))
            .collect()
    }

    Game {
        player_1: VecDeque::from_iter(parse_player(parts[0]).iter().cloned()),
        player_2: VecDeque::from_iter(parse_player(parts[1]).iter().cloned()),
    }
}

fn read_data(filename: &str) -> Game {
    let file = File::open(filename).expect("Could not find file.");
    let buffer = BufReader::new(file);
    let lines: Vec<String> = buffer
        .lines()
        .map(|s| s.expect("Could not read line."))
        .collect();
    parse_lines(&lines)
}

fn part1() {
    let mut game = read_data("input/day22.txt");
    while !game.step() {}
    let score = game.score();
    println!("{}", score);
}

fn part2() {
    let mut game = read_data("input/day22.txt");
    let deck1 = game.player_1.clone();
    let deck2 = game.player_2.clone();
    let (winner, cards) = recursive(deck1, deck2);
    match winner {
        Winner::Player1 => {
            game.player_1 = cards;
            game.player_2 = VecDeque::new();
        }
        Winner::Player2 => {
            game.player_1 = VecDeque::new();
            game.player_2 = cards;
        }
    }
    let score = game.score();
    println!("{}", score);
}

fn main() {
    timing(part1, 22, 1);
    timing(part2, 22, 2);
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_DATA: &[&str] = &[
        "Player 1:",
        "9",
        "2",
        "6",
        "3",
        "1",
        "",
        "Player 2:",
        "5",
        "8",
        "4",
        "7",
        "10",
    ];

    #[test]
    fn test_part_1_example_1() {
        let input: Vec<_> = TEST_DATA.iter().map(|s| s.to_string()).collect();
        let mut game = parse_lines(&input);
        let mut count: usize = 1;
        while !game.step() {
            count += 1;
        }
        assert_eq!(count, 29);
        let score = game.score();
        assert_eq!(score, 306);
    }

    #[test]
    fn test_part_2_example_2() {
        let input: Vec<_> = TEST_DATA.iter().map(|s| s.to_string()).collect();
        let mut game = parse_lines(&input);
        let deck1 = game.player_1.clone();
        let deck2 = game.player_2.clone();
        let (_, cards) = recursive(deck1, deck2);
        game.player_1 = VecDeque::new();
        game.player_2 = cards;
        let score = game.score();
        assert_eq!(score, 291);
    }
}
