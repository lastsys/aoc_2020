use common::performance::timing;
use regex::Regex;
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::ops::Add;

#[derive(Debug, PartialEq)]
enum Direction {
    NorthWest,
    NorthEast,
    East,
    West,
    SouthWest,
    SouthEast,
}

#[derive(Debug, Clone, PartialEq, Hash, Eq)]
struct Point {
    q: isize,
    r: isize,
}

impl Point {
    pub fn new(q: isize, r: isize) -> Point {
        Point { q, r }
    }
}

impl Add for &Point {
    type Output = Point;

    fn add(self, rhs: Self) -> Self::Output {
        Point {
            q: self.q + rhs.q,
            r: self.r + rhs.r,
        }
    }
}

#[derive(Debug)]
struct Floor {
    tiles: HashSet<Point>,
    turtle: Point,
}

impl Floor {
    pub fn new() -> Floor {
        Floor {
            tiles: HashSet::new(),
            turtle: Point::new(0, 0),
        }
    }

    pub fn walk_and_flip(&mut self, directions: &[Direction]) {
        self.turtle.q = 0;
        self.turtle.r = 0;
        for direction in directions.iter() {
            // Axial coordinates.
            // https://www.redblobgames.com/grids/hexagons/
            match direction {
                Direction::NorthWest => {
                    self.turtle.r -= 1;
                }
                Direction::NorthEast => {
                    self.turtle.q += 1;
                    self.turtle.r -= 1;
                }
                Direction::East => {
                    self.turtle.q += 1;
                }
                Direction::West => {
                    self.turtle.q -= 1;
                }
                Direction::SouthWest => {
                    self.turtle.q -= 1;
                    self.turtle.r += 1;
                }
                Direction::SouthEast => {
                    self.turtle.r += 1;
                }
            }
        }
        if self.tiles.contains(&self.turtle) {
            self.tiles.remove(&self.turtle);
        } else {
            self.tiles.insert(self.turtle.clone());
        }
    }

    pub fn count_black(&self) -> usize {
        self.tiles.len()
    }

    pub fn step(&mut self) {
        const NEIGHBOUR: [Point; 6] = [
            Point { q: 1, r: 0 },
            Point { q: -1, r: 0 },
            Point { q: 0, r: 1 },
            Point { q: 0, r: -1 },
            Point { q: 1, r: -1 },
            Point { q: -1, r: 1 },
        ];

        let mut next_tiles: HashSet<Point> = HashSet::new();
        let mut neighbour_count: HashMap<Point, usize> = HashMap::new();

        for p in self.tiles.iter() {
            for delta in &NEIGHBOUR {
                let neighbour = p + delta;
                neighbour_count
                    .entry(neighbour)
                    .and_modify(|count| *count += 1)
                    .or_insert(1);
            }
        }

        neighbour_count.drain().for_each(|(p, c)| {
            if self.tiles.contains(&p) {
                if c <= 2 {
                    next_tiles.insert(p);
                }
            } else if c == 2 {
                next_tiles.insert(p);
            }
        });

        self.tiles = next_tiles;
    }
}

fn parse_line(line: &str) -> Vec<Direction> {
    let re = Regex::new(r"e|se|sw|w|nw|ne").expect("Could not compile regex.");
    let directions = re
        .find_iter(line)
        .map(|next| match next.as_str() {
            "e" => Direction::East,
            "se" => Direction::SouthEast,
            "sw" => Direction::SouthWest,
            "w" => Direction::West,
            "nw" => Direction::NorthWest,
            "ne" => Direction::NorthEast,
            _ => unreachable!(),
        })
        .collect();
    directions
}

fn parse_instructions(input: &[&str]) -> Vec<Vec<Direction>> {
    input.iter().fold(Vec::new(), |mut v, line| {
        let directions = parse_line(line);
        v.push(directions);
        v
    })
}

fn read_data(filename: &str) -> Vec<Vec<Direction>> {
    let file = File::open(filename).expect("Could not find file.");
    let buffer = BufReader::new(file);
    let lines: Vec<String> = buffer
        .lines()
        .map(|s| s.expect("Could not read line."))
        .collect();
    let str_lines: Vec<&str> = lines.iter().map(|s| s.as_str()).collect();
    parse_instructions(&str_lines)
}

fn part1() {
    let directions = read_data("input/day24.txt");
    let mut floor: Floor = Floor::new();
    for d in directions.iter() {
        floor.walk_and_flip(d);
    }
    println!("{}", floor.count_black());
}

fn part2() {
    let directions = read_data("input/day24.txt");
    let mut floor: Floor = Floor::new();
    for d in directions.iter() {
        floor.walk_and_flip(d);
    }
    for _ in 0..100 {
        floor.step();
    }
    println!("{}", floor.count_black());
}

fn main() {
    timing(part1, 24, 1);
    timing(part2, 24, 2);
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_DATA: &[&str] = &[
        "sesenwnenenewseeswwswswwnenewsewsw",
        "neeenesenwnwwswnenewnwwsewnenwseswesw",
        "seswneswswsenwwnwse",
        "nwnwneseeswswnenewneswwnewseswneseene",
        "swweswneswnenwsewnwneneseenw",
        "eesenwseswswnenwswnwnwsewwnwsene",
        "sewnenenenesenwsewnenwwwse",
        "wenwwweseeeweswwwnwwe",
        "wsweesenenewnwwnwsenewsenwwsesesenwne",
        "neeswseenwwswnwswswnw",
        "nenwswwsewswnenenewsenwsenwnesesenew",
        "enewnwewneswsewnwswenweswnenwsenwsw",
        "sweneswneswneneenwnewenewwneswswnese",
        "swwesenesewenwneswnwwneseswwne",
        "enesenwswwswneneswsenwnewswseenwsese",
        "wnwnesenesenenwwnenwsewesewsesesew",
        "nenewswnwewswnenesenwnesewesw",
        "eneswnwswnwsenenwnwnwwseeswneewsenese",
        "neswnwewnwnwseenwseesewsenwsweewe",
        "wseweeenwnesenwwwswnew",
    ];

    #[test]
    fn test_part_1_parse() {
        let directions = parse_instructions(TEST_DATA);
        assert_eq!(directions.len(), 20);

        // "se se nw ne ne ne w se e sw w sw sw w ne ne w se w sw"
        assert_eq!(
            directions[0],
            vec![
                Direction::SouthEast,
                Direction::SouthEast,
                Direction::NorthWest,
                Direction::NorthEast,
                Direction::NorthEast,
                Direction::NorthEast,
                Direction::West,
                Direction::SouthEast,
                Direction::East,
                Direction::SouthWest,
                Direction::West,
                Direction::SouthWest,
                Direction::SouthWest,
                Direction::West,
                Direction::NorthEast,
                Direction::NorthEast,
                Direction::West,
                Direction::SouthEast,
                Direction::West,
                Direction::SouthWest
            ]
        );
    }

    #[test]
    fn test_part_1_example_1() {
        let directions = parse_instructions(TEST_DATA);
        let mut floor: Floor = Floor::new();
        for d in directions.iter() {
            floor.walk_and_flip(d);
        }
        assert_eq!(floor.count_black(), 10);
    }
}
