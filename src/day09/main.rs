use common::input::fold_input;
use common::performance::timing;
use std::collections::HashSet;
use std::io::Error;

#[derive(Clone)]
struct Xmas {
    pub sums: HashSet<usize>,
    pub pos: usize,
    pub values: Vec<usize>,
}

impl Xmas {
    pub fn new(preamble: Vec<usize>) -> Xmas {
        let mut xmas = Xmas {
            sums: HashSet::new(),
            pos: preamble.len(),
            values: preamble,
        };
        for v1 in &xmas.values {
            for v2 in &xmas.values {
                if v1 != v2 {
                    xmas.sums.insert(v1 + v2);
                }
            }
        }
        xmas
    }

    pub fn step(&mut self, value: usize) -> bool {
        self.pos += 1;
        for v in &self.values {
            if *v != value {
                self.sums.insert(*v + value);
            }
        }
        self.values.push(value);

        self.sums.contains(&value)
    }
}

fn input_folder(container: &mut Vec<String>, line: Result<String, Error>) -> &mut Vec<String> {
    container.push(line.expect("Could not read line.").trim().to_string());
    container
}

fn part1() {
    let mut init: Vec<String> = Vec::new();
    let input: &Vec<String> =
        fold_input("input/day09.txt", &mut init, input_folder).expect("Could not read file.");
    let values: Vec<usize> = input
        .iter()
        .map(|s| s.parse::<usize>().expect("Could not parse number."))
        .collect();
    let preamble: Vec<usize> = values.iter().take(25).copied().collect();
    let mut iterator = values.iter().skip(25);
    let mut xmas = Xmas::new(preamble);
    loop {
        match iterator.next() {
            Some(v) => {
                if !xmas.step(*v) {
                    println!("first invalid = {}", v);
                    break;
                }
            }
            None => panic!("End of sequence!"),
        }
    }
}

fn find_weakness(input: &[usize], target: usize) -> Option<usize> {
    for i1 in 0..(input.len() - 2) {
        for length in 2..(input.len() - i1) {
            let values: Vec<usize> = input[i1..].iter().take(length).copied().collect();
            if values.iter().sum::<usize>() == target {
                let v1 = values.iter().min().expect("Could not find minimum.");
                let v2 = values.iter().max().expect("Could not find maximum.");
                return Some(v1 + v2);
            }
        }
    }
    None
}

fn part2() {
    let mut init: Vec<String> = Vec::new();
    let input: &Vec<String> =
        fold_input("input/day09.txt", &mut init, input_folder).expect("Could not read file.");
    let values: Vec<usize> = input
        .iter()
        .map(|s| s.parse::<usize>().expect("Could not parse number."))
        .collect();

    let weakness = find_weakness(&values, 14144619);
    println!("Weakness = {:?}", weakness);
}

fn main() {
    timing(part1, 9, 1);
    timing(part2, 9, 2);
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_DATA_1: &[usize] = &[
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
    ];

    const TEST_DATA_2: &[usize] = &[
        35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309, 576,
    ];

    #[test]
    fn test_example1() {
        let xmas = Xmas::new(Vec::from(TEST_DATA_1));
        assert!(xmas.sums.contains(&26usize));
        assert!(xmas.sums.contains(&49usize));
        assert!(!xmas.sums.contains(&50usize));
        assert!(!xmas.sums.contains(&100usize));

        let mut xmas1 = xmas.clone();
        assert!(xmas1.step(45));

        let mut xmas2 = xmas1.clone();
        assert!(xmas2.step(26));
    }

    #[test]
    fn test_example2() {
        let test_data: Vec<usize> = TEST_DATA_2[0..5].iter().map(|v| *v).collect();
        let mut xmas = Xmas::new(test_data);
        for v in TEST_DATA_2[5..].iter() {
            if !xmas.step(*v) {
                assert_eq!(*v, 127);
                break;
            };
        }
    }

    #[test]
    fn test_example3() {
        let weakness = find_weakness(TEST_DATA_2, 127);
        assert_eq!(weakness, Some(62));
    }
}
