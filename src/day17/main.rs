use common::performance::timing;
use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::iter::FromIterator;

type Coordinate = (isize, isize, isize);
type Grid = HashSet<Coordinate>;

type Coordinate2 = (isize, isize, isize, isize);
type Grid2 = HashSet<Coordinate2>;

fn read_data() -> Grid {
    let mut grid: Grid = Grid::new();
    let file = File::open("input/day17.txt").expect("Could not open file.");
    let reader = BufReader::new(file);
    reader.lines().enumerate().for_each(|(row, line)| {
        line.expect("Could not read line.")
            .chars()
            .enumerate()
            .for_each(|(col, cell)| {
                if cell == '#' {
                    let x = col as isize;
                    let y = row as isize;
                    let state: Coordinate = (x, y, 0isize);
                    grid.insert(state);
                }
            })
    });
    grid
}

fn read_data2() -> Grid2 {
    let mut grid: Grid2 = Grid2::new();
    let file = File::open("input/day17.txt").expect("Could not open file.");
    let reader = BufReader::new(file);
    reader.lines().enumerate().for_each(|(row, line)| {
        line.expect("Could not read line.")
            .chars()
            .enumerate()
            .for_each(|(col, cell)| {
                if cell == '#' {
                    let x = col as isize;
                    let y = row as isize;
                    let state: Coordinate2 = (x, y, 0isize, 0isize);
                    grid.insert(state);
                }
            })
    });
    grid
}

fn count_active_neighbors(grid: &Grid, coord: &Coordinate) -> usize {
    let mut neighbors = 0usize;
    for dx in -1..=1 {
        for dy in -1..=1 {
            for dz in -1..=1 {
                if !(dx == 0 && dy == 0 && dz == 0)
                    && grid.contains(&(coord.0 + dx, coord.1 + dy, coord.2 + dz))
                {
                    neighbors += 1;
                }
            }
        }
    }
    neighbors
}

fn count_active_neighbors_2(grid: &Grid2, coord: &Coordinate2) -> usize {
    let mut neighbors = 0usize;
    for dx in -1..=1 {
        for dy in -1..=1 {
            for dz in -1..=1 {
                for dw in -1..=1 {
                    if !(dx == 0 && dy == 0 && dz == 0 && dw == 0)
                        && grid.contains(&(coord.0 + dx, coord.1 + dy, coord.2 + dz, coord.3 + dw))
                    {
                        neighbors += 1;
                    }
                }
            }
        }
    }
    neighbors
}

fn step(grid: &Grid) -> Grid {
    let mut next_grid = Grid::new();
    let mut all_neighbors: Grid = Grid::new();
    // Find all empty cells to check.
    for coord in grid {
        for dx in -1..=1 {
            for dy in -1..=1 {
                for dz in -1..=1 {
                    if !(dx == 0 && dy == 0 && dz == 0) {
                        all_neighbors.insert((coord.0 + dx, coord.1 + dy, coord.2 + dz));
                    }
                }
            }
        }
    }
    let empty_to_check: Grid = HashSet::from_iter(all_neighbors.difference(&grid).copied());

    // Apply rules for enabled.
    for coord in grid {
        let neighbors = count_active_neighbors(&grid, &coord);
        if neighbors == 2 || neighbors == 3 {
            next_grid.insert(*coord);
        }
    }

    // Apply rules for disabled neighbors.
    for coord in empty_to_check {
        let neighbors = count_active_neighbors(&grid, &coord);
        if neighbors == 3 {
            next_grid.insert(coord);
        }
    }

    next_grid
}

fn step2(grid: &Grid2) -> Grid2 {
    let mut next_grid = Grid2::new();
    let mut all_neighbors: Grid2 = Grid2::new();
    // Find all empty cells to check.
    for coord in grid {
        for dx in -1..=1 {
            for dy in -1..=1 {
                for dz in -1..=1 {
                    for dw in -1..=1 {
                        if !(dx == 0 && dy == 0 && dz == 0 && dw == 0) {
                            all_neighbors.insert((
                                coord.0 + dx,
                                coord.1 + dy,
                                coord.2 + dz,
                                coord.3 + dw,
                            ));
                        }
                    }
                }
            }
        }
    }
    let empty_to_check: Grid2 = HashSet::from_iter(all_neighbors.difference(&grid).copied());

    // Apply rules for enabled.
    for coord in grid {
        let neighbors = count_active_neighbors_2(&grid, &coord);
        if neighbors == 2 || neighbors == 3 {
            next_grid.insert(*coord);
        }
    }

    // Apply rules for disabled neighbors.
    for coord in empty_to_check {
        let neighbors = count_active_neighbors_2(&grid, &coord);
        if neighbors == 3 {
            next_grid.insert(coord);
        }
    }

    next_grid
}

fn part1() {
    let mut grid = read_data();

    for _ in 0..6 {
        grid = step(&grid);
    }

    println!("{}", grid.len());
}

fn part2() {
    let mut grid = read_data2();

    for _ in 0..6 {
        grid = step2(&grid);
    }

    println!("{}", grid.len());
}

fn main() {
    timing(part1, 17, 1);
    timing(part2, 17, 2);
}

#[cfg(test)]
mod test {
    use super::*;
    use std::iter::FromIterator;

    #[test]
    fn test_example_1() {
        // Before any cycles:
        //
        // z=0
        // .#.
        // ..#
        // ###
        let grid_0: Grid = Grid::from_iter(
            [(1, 0, 0), (2, 1, 0), (0, 2, 0), (1, 2, 0), (2, 2, 0)]
                .iter()
                .cloned(),
        );

        // z == 0
        assert_eq!(count_active_neighbors(&grid_0, &(0, 0, 0)), 1);
        assert_eq!(count_active_neighbors(&grid_0, &(1, 0, 0)), 1);
        assert_eq!(count_active_neighbors(&grid_0, &(2, 0, 0)), 2);

        assert_eq!(count_active_neighbors(&grid_0, &(0, 1, 0)), 3);
        assert_eq!(count_active_neighbors(&grid_0, &(1, 1, 0)), 5);
        assert_eq!(count_active_neighbors(&grid_0, &(2, 1, 0)), 3);

        assert_eq!(count_active_neighbors(&grid_0, &(0, 2, 0)), 1);
        assert_eq!(count_active_neighbors(&grid_0, &(1, 2, 0)), 3);
        assert_eq!(count_active_neighbors(&grid_0, &(2, 2, 0)), 2);

        // z == 1
        assert_eq!(count_active_neighbors(&grid_0, &(0, 0, 1)), 1);
        assert_eq!(count_active_neighbors(&grid_0, &(1, 0, 1)), 2);
        assert_eq!(count_active_neighbors(&grid_0, &(2, 0, 1)), 2);

        assert_eq!(count_active_neighbors(&grid_0, &(0, 1, 1)), 3);
        assert_eq!(count_active_neighbors(&grid_0, &(1, 1, 1)), 5);
        assert_eq!(count_active_neighbors(&grid_0, &(2, 1, 1)), 4);

        assert_eq!(count_active_neighbors(&grid_0, &(0, 2, 1)), 2);
        assert_eq!(count_active_neighbors(&grid_0, &(1, 2, 1)), 4);
        assert_eq!(count_active_neighbors(&grid_0, &(2, 2, 1)), 3);

        // z == -1
        assert_eq!(count_active_neighbors(&grid_0, &(0, 0, -1)), 1);
        assert_eq!(count_active_neighbors(&grid_0, &(1, 0, -1)), 2);
        assert_eq!(count_active_neighbors(&grid_0, &(2, 0, -1)), 2);

        assert_eq!(count_active_neighbors(&grid_0, &(0, 1, -1)), 3);
        assert_eq!(count_active_neighbors(&grid_0, &(1, 1, -1)), 5);
        assert_eq!(count_active_neighbors(&grid_0, &(2, 1, -1)), 4);

        assert_eq!(count_active_neighbors(&grid_0, &(0, 2, -1)), 2);
        assert_eq!(count_active_neighbors(&grid_0, &(1, 2, -1)), 4);
        assert_eq!(count_active_neighbors(&grid_0, &(2, 2, -1)), 3);
    }
}
