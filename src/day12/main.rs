use common::input::fold_input;
use common::performance::timing;
use std::io::Error;
use std::iter::FromIterator;

#[derive(Debug, PartialEq)]
enum Instruction {
    MoveNorth(isize),
    MoveSouth(isize),
    MoveWest(isize),
    MoveEast(isize),
    MoveForward(isize),
    TurnLeft(isize),
    TurnRight(isize),
}

struct State {
    pub direction: (isize, isize),
    pub position: (isize, isize),
    pub waypoint: (isize, isize),
}

impl State {
    pub fn new() -> State {
        State {
            direction: (1, 0),
            position: (0, 0),
            waypoint: (10, -1),
        }
    }

    pub fn step(&mut self, instruction: &Instruction) {
        match instruction {
            Instruction::MoveForward(steps) => {
                self.position = (
                    self.position.0 + self.direction.0 * *steps,
                    self.position.1 + self.direction.1 * *steps,
                );
            }
            Instruction::MoveNorth(steps) => {
                self.position = (self.position.0, self.position.1 - *steps)
            }
            Instruction::MoveEast(steps) => {
                self.position = (self.position.0 + *steps, self.position.1)
            }
            Instruction::MoveSouth(steps) => {
                self.position = (self.position.0, self.position.1 + *steps)
            }
            Instruction::MoveWest(steps) => {
                self.position = (self.position.0 - *steps, self.position.1)
            }
            Instruction::TurnLeft(degrees) => {
                let steps = (*degrees % 360) / 90;
                for _ in 0..steps {
                    // Rotation matrix for 270 degrees (-90 degrees)
                    // [0,1;-1,0]
                    self.direction = (self.direction.1, -self.direction.0);
                }
            }
            Instruction::TurnRight(degrees) => {
                let steps = (degrees % 360) / 90;
                for _ in 0..steps {
                    // Rotation matrix for 90 degrees:
                    // [0,-1;1,0]
                    self.direction = (-self.direction.1, self.direction.0);
                }
            }
        }
    }

    pub fn step2(&mut self, instruction: &Instruction) {
        match instruction {
            Instruction::MoveForward(steps) => {
                self.position = (
                    self.position.0 + self.waypoint.0 * *steps,
                    self.position.1 + self.waypoint.1 * *steps,
                );
            }
            Instruction::MoveNorth(steps) => {
                self.waypoint = (self.waypoint.0, self.waypoint.1 - *steps);
            }
            Instruction::MoveEast(steps) => {
                self.waypoint = (self.waypoint.0 + *steps, self.waypoint.1);
            }
            Instruction::MoveSouth(steps) => {
                self.waypoint = (self.waypoint.0, self.waypoint.1 + *steps);
            }
            Instruction::MoveWest(steps) => {
                self.waypoint = (self.waypoint.0 - *steps, self.waypoint.1);
            }
            Instruction::TurnLeft(degrees) => {
                let steps = (*degrees % 360) / 90;
                for _ in 0..steps {
                    // Rotation matrix for 270 degrees (-90 degrees)
                    // [0,1;-1,0]
                    self.waypoint = (self.waypoint.1, -self.waypoint.0);
                }
            }
            Instruction::TurnRight(degrees) => {
                let steps = (*degrees % 360) / 90;
                for _ in 0..steps {
                    // Rotation matrix for 90 degrees:
                    // [0,-1;1,0]
                    self.waypoint = (-self.waypoint.1, self.waypoint.0);
                }
            }
        }
    }
}

fn input_folder(
    container: &mut Vec<Instruction>,
    line: Result<String, Error>,
) -> &mut Vec<Instruction> {
    container.push(parse_instruction(
        line.expect("Could not read line.").trim(),
    ));
    container
}

fn parse_instruction(instruction: &str) -> Instruction {
    let action: char = instruction
        .chars()
        .next()
        .expect("Could not extract action.");
    let value: isize = {
        let value_chars: Vec<char> = instruction.chars().skip(1).collect();
        let joined = String::from_iter(value_chars.iter());
        joined.parse().expect("Could not parse value.")
    };
    match (action, value) {
        ('N', steps) => Instruction::MoveNorth(steps),
        ('S', steps) => Instruction::MoveSouth(steps),
        ('W', steps) => Instruction::MoveWest(steps),
        ('E', steps) => Instruction::MoveEast(steps),
        ('F', steps) => Instruction::MoveForward(steps),
        ('L', degrees) => Instruction::TurnLeft(degrees),
        ('R', degrees) => Instruction::TurnRight(degrees),
        _ => panic!("Invalid instruction."),
    }
}

fn part1() {
    let mut init: Vec<Instruction> = Vec::new();
    let instructions: &Vec<Instruction> =
        fold_input("input/day12.txt", &mut init, input_folder).expect("Could not read file.");
    let final_state = instructions
        .iter()
        .fold(State::new(), |mut state, instruction| {
            state.step(instruction);
            state
        });
    println!(
        "{}, {} => {}",
        final_state.position.0,
        final_state.position.1,
        final_state.position.0.abs() + final_state.position.1.abs()
    );
}

fn part2() {
    let mut init: Vec<Instruction> = Vec::new();
    let instructions: &Vec<Instruction> =
        fold_input("input/day12.txt", &mut init, input_folder).expect("Could not read file.");
    let final_state = instructions
        .iter()
        .fold(State::new(), |mut state, instruction| {
            state.step2(instruction);
            state
        });
    println!(
        "{}, {} => {}",
        final_state.position.0,
        final_state.position.1,
        final_state.position.0.abs() + final_state.position.1.abs()
    );
}

fn main() {
    timing(part1, 12, 1);
    timing(part2, 12, 2);
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_DATA: &[&str] = &["F10", "N3", "F7", "R90", "F11"];

    #[test]
    fn test_parse_instruction() {
        assert_eq!(parse_instruction("F10"), Instruction::MoveForward(10));
        assert_eq!(parse_instruction("N3"), Instruction::MoveNorth(3));
        assert_eq!(parse_instruction("S4"), Instruction::MoveSouth(4));
        assert_eq!(parse_instruction("R90"), Instruction::TurnRight(90));
        assert_eq!(parse_instruction("L180"), Instruction::TurnLeft(180));
        assert_eq!(parse_instruction("E5"), Instruction::MoveEast(5));
        assert_eq!(parse_instruction("W6"), Instruction::MoveWest(6));
    }

    #[test]
    fn test_part_1_example_1() {
        let instructions: Vec<Instruction> =
            TEST_DATA.iter().map(|s| parse_instruction(s)).collect();
        let mut state = State::new();

        state.step(&instructions[0]);
        assert_eq!(state.direction, (1, 0));
        assert_eq!(state.position, (10, 0));

        state.step(&instructions[1]);
        assert_eq!(state.direction, (1, 0));
        assert_eq!(state.position, (10, -3));

        state.step(&instructions[2]);
        assert_eq!(state.direction, (1, 0));
        assert_eq!(state.position, (17, -3));

        state.step(&instructions[3]);
        assert_eq!(state.direction, (0, 1));
        assert_eq!(state.position, (17, -3));

        state.step(&instructions[4]);
        assert_eq!(state.direction, (0, 1));
        assert_eq!(state.position, (17, 8));
    }

    #[test]
    fn test_part_2_example_2() {
        let instructions: Vec<Instruction> =
            TEST_DATA.iter().map(|s| parse_instruction(s)).collect();
        let mut state = State::new();

        state.step2(&instructions[0]);
        assert_eq!(state.waypoint, (10, -1));
        assert_eq!(state.position, (100, -10));

        state.step2(&instructions[1]);
        assert_eq!(state.waypoint, (10, -4));
        assert_eq!(state.position, (100, -10));

        state.step2(&instructions[2]);
        assert_eq!(state.waypoint, (10, -4));
        assert_eq!(state.position, (170, -38));

        state.step2(&instructions[3]);
        assert_eq!(state.waypoint, (4, 10));
        assert_eq!(state.position, (170, -38));

        state.step2(&instructions[4]);
        assert_eq!(state.waypoint, (4, 10));
        assert_eq!(state.position, (214, 72));
    }
}
