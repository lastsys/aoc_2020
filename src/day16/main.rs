use common::performance::timing;
use regex::Regex;
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::iter::FromIterator;

#[derive(Debug)]
struct TicketData {
    #[allow(clippy::type_complexity)]
    range: HashMap<String, ((usize, usize), (usize, usize))>,
    your_ticket: Vec<usize>,
    tickets: Vec<Vec<usize>>,
}

impl TicketData {
    pub fn new() -> TicketData {
        TicketData {
            range: HashMap::new(),
            your_ticket: Vec::new(),
            tickets: Vec::new(),
        }
    }
}

enum ParseMode {
    Preamble,
    YourTicket,
    NearbyTicket,
}

fn parse_data(input: &[String]) -> TicketData {
    let range_pattern =
        Regex::new(r"(?P<name>[^:]+):\s(?P<r1>\d+)-(?P<r2>\d+)\sor\s(?P<r3>\d+)-(?P<r4>\d+)")
            .expect("Failed to compile regex.");

    let mut ticket_data = TicketData::new();
    let mut parse_mode = ParseMode::Preamble;

    for line in input.iter() {
        let pattern = range_pattern.captures(line);
        match (pattern, &parse_mode) {
            (None, _) if line == "nearby tickets:" => parse_mode = ParseMode::NearbyTicket,
            (None, _) if line == "your ticket:" => parse_mode = ParseMode::YourTicket,
            (None, _) if line.is_empty() => (),
            (Some(captures), ParseMode::Preamble) => {
                let name = captures
                    .name("name")
                    .expect("Could not find name field.")
                    .as_str()
                    .to_string();
                let r1 = captures
                    .name("r1")
                    .expect("Could not find r1 field.")
                    .as_str()
                    .parse::<usize>()
                    .expect("Could not parse r1 value.");
                let r2 = captures
                    .name("r2")
                    .expect("Could not find r2 field.")
                    .as_str()
                    .parse::<usize>()
                    .expect("Could not parse r2 value.");
                let r3 = captures
                    .name("r3")
                    .expect("Could not find r3 field.")
                    .as_str()
                    .parse::<usize>()
                    .expect("Could not parse r3 value.");
                let r4 = captures
                    .name("r4")
                    .expect("Could not find r4 field.")
                    .as_str()
                    .parse::<usize>()
                    .expect("Could not parse r4 value.");
                ticket_data.range.insert(name, ((r1, r2), (r3, r4)));
            }
            _ => {
                let values: Vec<usize> = line
                    .split(',')
                    .map(|s| s.parse::<usize>().expect("Failed to parse number."))
                    .collect();
                match parse_mode {
                    ParseMode::YourTicket => {
                        ticket_data.your_ticket = values;
                    }
                    ParseMode::NearbyTicket => {
                        ticket_data.tickets.push(values);
                    }
                    _ => panic!("Parse failure."),
                }
            }
        }
    }
    ticket_data
}

fn read_data() -> Vec<String> {
    let file = File::open("input/day16.txt").expect("Could not open file.");
    let reader = BufReader::new(file);
    reader
        .lines()
        .map(|line| line.expect("Could not read line."))
        .collect::<Vec<String>>()
}

fn is_valid(value: &usize, ticket_data: &TicketData) -> bool {
    ticket_data.range.iter().any(|(_, ((r1, r2), (r3, r4)))| {
        ((value >= r1) && (value <= r2)) || ((value >= r3) && (value <= r4))
    })
}

fn find_missing_values(ticket_data: &TicketData) -> Vec<usize> {
    let mut missing: Vec<usize> = Vec::new();
    for ticket in ticket_data.tickets.iter() {
        for value in ticket {
            if !is_valid(value, &ticket_data) {
                missing.push(*value);
            }
        }
    }
    missing
}

fn part1() {
    let input = read_data();
    let ticket_data = parse_data(&input);
    let missing = find_missing_values(&ticket_data);
    println!("{}", missing.iter().sum::<usize>());
}

fn match_columns(ticket_data: &TicketData) -> HashMap<usize, String> {
    let valid_tickets: Vec<&Vec<usize>> = ticket_data
        .tickets
        .iter()
        .filter(|t| t.iter().all(|value| is_valid(value, ticket_data)))
        .collect();

    let n = valid_tickets.len();

    let transpose = {
        let mut columns: Vec<Vec<usize>> = Vec::with_capacity(n);
        for col in 0..ticket_data.your_ticket.len() {
            let mut column: Vec<usize> = Vec::with_capacity(n);
            for ticket in valid_tickets.iter() {
                column.push(ticket[col]);
            }
            columns.push(column);
        }
        columns
    };
    assert_eq!(transpose.len(), ticket_data.your_ticket.len());

    // Find out which columns each range match.
    let mut range_per_column: HashMap<String, HashSet<usize>> = HashMap::new();
    for (name, ((r1, r2), (r3, r4))) in ticket_data.range.iter() {
        for (i, column) in transpose.iter().enumerate() {
            let is_match = column
                .iter()
                .all(|value| ((value >= r1) && (value <= r2)) || ((value >= r3) && (value <= r4)));
            if is_match {
                range_per_column
                    .entry(name.to_owned())
                    .and_modify(|e| {
                        e.insert(i);
                    })
                    .or_insert({
                        let mut set = HashSet::new();
                        set.insert(i);
                        set
                    });
            }
        }
    }

    // Now reduce.
    let mut column_to_range: HashMap<usize, String> = HashMap::new();
    while !range_per_column.is_empty() {
        // Find the column with only one matching range.
        let (name, matching_columns) = range_per_column
            .iter()
            .find(|(_, v)| v.len() == 1)
            .expect("Could not find next element.");
        assert_eq!(matching_columns.len(), 1);
        let column = *Vec::from_iter(matching_columns.iter())[0];
        column_to_range.insert(column, name.to_owned().to_string());
        let rpc = range_per_column.clone();
        for (key, set) in rpc.iter() {
            if set.len() == 1 {
                // Remove key if this is the last one.
                range_per_column.remove(key);
            } else {
                // Otherwise remove column.
                range_per_column.entry(key.to_owned()).and_modify(|s| {
                    s.remove(&column);
                });
            }
        }
    }

    column_to_range
}

fn part2() {
    let input = read_data();
    let ticket_data = parse_data(&input);
    let matches = match_columns(&ticket_data);
    let prod: usize = matches
        .iter()
        .filter(|(_, name)| name.starts_with("departure"))
        .map(|(column, _)| ticket_data.your_ticket[*column])
        .product();
    println!("{}", prod);
}

fn main() {
    timing(part1, 16, 1);
    timing(part2, 16, 2);
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_DATA: &[&str] = &[
        "class: 1-3 or 5-7",
        "row: 6-11 or 33-44",
        "seat: 13-40 or 45-50",
        "",
        "your ticket:",
        "7,1,14",
        "",
        "nearby tickets:",
        "7,3,47",
        "40,4,50",
        "55,2,20",
        "38,6,12",
    ];

    #[test]
    fn test_parse() {
        let test_data: Vec<String> = TEST_DATA.iter().map(|s| s.to_string()).collect();
        let ticket_data = parse_data(&test_data);
        assert_eq!(ticket_data.range.len(), 3);
        assert_eq!(ticket_data.your_ticket.len(), 3);
        assert_eq!(ticket_data.tickets.len(), 4);
    }

    #[test]
    fn test_example_1() {
        let test_data: Vec<String> = TEST_DATA.iter().map(|s| s.to_string()).collect();
        let ticket_data = parse_data(&test_data);
        assert_eq!(ticket_data.range.len(), 3);

        let missing = find_missing_values(&ticket_data);
        assert_eq!(missing.len(), 3);
        assert!(missing.contains(&4));
        assert!(missing.contains(&55));
        assert!(missing.contains(&12));
        assert_eq!(missing.iter().sum::<usize>(), 71);
    }

    #[test]
    fn test_real_data() {
        let input = read_data();
        let ticket_data = parse_data(&input);
        assert_eq!(ticket_data.range.len(), 20);
        assert_eq!(ticket_data.your_ticket.len(), 20);
        assert_eq!(ticket_data.tickets.len(), 262 - 25);
        assert_eq!(
            ticket_data.tickets[0],
            vec![
                153, 109, 923, 689, 426, 793, 483, 628, 843, 774, 785, 841, 63, 168, 314, 725, 489,
                339, 231, 914,
            ]
        );
        assert_eq!(
            ticket_data.tickets[1],
            vec![
                177, 714, 226, 83, 177, 199, 186, 227, 474, 942, 978, 440, 905, 346, 788, 700, 346,
                247, 925, 825,
            ]
        );
    }

    const TEST_DATA_2: &[&str] = &[
        "class: 0-1 or 4-19",
        "row: 0-5 or 8-19",
        "seat: 0-13 or 16-19",
        "",
        "your ticket:",
        "11,12,13",
        "",
        "nearby tickets:",
        "3,9,18",
        "15,1,5",
        "5,14,9",
    ];

    #[test]
    fn test_example_2() {
        let input: Vec<String> = TEST_DATA_2.iter().map(|s| s.to_string()).collect();
        let ticket_data = parse_data(&input);
        let matches = match_columns(&ticket_data);
        assert_eq!(matches.len(), 3);
        assert_eq!(matches[&1], "class");
        assert_eq!(matches[&0], "row");
        assert_eq!(matches[&2], "seat");
    }
}
