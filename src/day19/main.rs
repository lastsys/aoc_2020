use common::performance::timing;
use regex::Regex;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::str::FromStr;

type RuleId = usize;

#[derive(Debug, PartialEq)]
enum RuleType {
    Seq(Vec<RuleId>),
    Or(Vec<RuleId>, Vec<RuleId>),
    Character(char),
}

#[derive(Debug, PartialEq)]
struct Rule {
    id: RuleId,
    rule: RuleType,
}

#[derive(Debug)]
struct ParseRuleError;

impl FromStr for Rule {
    type Err = ParseRuleError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let format_1 = Regex::new(r#"(?P<id>\d+): "(?P<char>\w)""#).expect("Invalid regex 1.");
        let format_2 = Regex::new(r"(?P<id>\d+): (?P<left>[\s\d]+)\|(?P<right>[\s\d]+)")
            .expect("Invalid regex 2.");
        let format_3 = Regex::new(r"(?P<id>\d+): (?P<num>[\s\d]+)").expect("Invalid regex 3.");

        if let Some(capture_1) = format_1.captures(s) {
            let id = capture_1
                .name("id")
                .expect("Could not match id.")
                .as_str()
                .parse::<RuleId>()
                .expect("Could not parse RuleId.");
            let character = capture_1
                .name("char")
                .expect("Could not parse char.")
                .as_str()
                .chars()
                .next()
                .expect("Could not extract char.");
            Ok(Rule {
                id,
                rule: RuleType::Character(character),
            })
        } else if let Some(capture_2) = format_2.captures(s) {
            let id = capture_2
                .name("id")
                .expect("Could not match id.")
                .as_str()
                .parse::<RuleId>()
                .expect("Could not parse RuleId.");
            let left: Vec<RuleId> = capture_2
                .name("left")
                .expect("Could not match left.")
                .as_str()
                .trim()
                .split_whitespace()
                .map(|s| {
                    s.parse::<RuleId>()
                        .expect("Could not parse rule id in left part.")
                })
                .collect();
            let right = capture_2
                .name("right")
                .expect("Could not match right.")
                .as_str()
                .trim()
                .split_whitespace()
                .map(|s| {
                    s.parse::<RuleId>()
                        .expect("Could not parse rule id in right part.")
                })
                .collect::<Vec<RuleId>>();
            Ok(Rule {
                id,
                rule: RuleType::Or(left, right),
            })
        } else if let Some(capture_3) = format_3.captures(s) {
            let id = capture_3
                .name("id")
                .expect("Could not match id.")
                .as_str()
                .parse::<RuleId>()
                .expect("Could not parse RuleId.");
            let rules = capture_3
                .name("num")
                .expect("Could not match left.")
                .as_str()
                .trim()
                .split_whitespace()
                .map(|s| {
                    s.parse::<RuleId>()
                        .expect("Could not parse rule id in left part.")
                })
                .collect::<Vec<RuleId>>();
            Ok(Rule {
                id,
                rule: RuleType::Seq(rules),
            })
        } else {
            Err(ParseRuleError)
        }
    }
}

fn parse_rules(input: &[&str]) -> HashMap<RuleId, Rule> {
    let mut rules: HashMap<RuleId, Rule> = HashMap::new();
    input.iter().for_each(|line| {
        let rule = line.parse::<Rule>().expect("Failed to parse rule.");
        rules.insert(rule.id, rule);
    });
    rules
}

fn read_input(filename: &str) -> (Vec<String>, Vec<String>) {
    let file = File::open(filename).expect("Could not find file.");
    let buffer = BufReader::new(file);
    let mut read_mode = 0;
    let mut rules: Vec<String> = Vec::new();
    let mut messages: Vec<String> = Vec::new();
    for line_result in buffer.lines() {
        let line = line_result.expect("Could not read line.");
        if line.is_empty() {
            read_mode = 1;
            continue;
        }
        match read_mode {
            0 => {
                rules.push(line);
            }
            1 => {
                messages.push(line);
            }
            _ => panic!("Invalid read mode."),
        }
    }
    (rules, messages)
}

fn part1() {
    let (rules, messages) = read_input("input/day19.txt");
    let str_rules: Vec<&str> = rules.iter().map(|s| s.as_str()).collect();
    let parsed_rules = parse_rules(&str_rules);

    let expr = format!("^{}$", build_regex(&parsed_rules, 0, 0));

    match Regex::new(expr.as_str()) {
        Ok(r) => {
            let valid = messages.iter().filter(|m| r.is_match(m)).count();
            println!("{}", valid);
        }
        Err(e) => {
            println!("{:?}", e);
        }
    };
}

fn body(rules: &HashMap<RuleId, Rule>, v: &[RuleId], n: usize) -> String {
    v.iter()
        .map(|rule_id| build_regex(rules, *rule_id, n + 1))
        .fold(String::new(), |total_string, s| {
            format!("{}{}", total_string, s)
        })
}

fn build_regex(rules: &HashMap<RuleId, Rule>, rule: RuleId, n: usize) -> String {
    if n > 42 {
        return String::from("");
    }
    match &rules[&rule].rule {
        RuleType::Character(c) => c.to_string(),
        RuleType::Seq(v) => {
            let b = body(rules, v, n + 1);
            if !b.is_empty() {
                format!("({})", body(rules, v, n + 1))
            } else {
                String::from("")
            }
        }
        RuleType::Or(left, right) => {
            let left = body(rules, left, n + 1);
            let right = body(rules, right, n + 1);
            if !left.is_empty() {
                format!("({}|{})", left, right)
            } else {
                String::from("")
            }
        }
    }
}

fn change_rules(rules: &mut HashMap<usize, Rule>) {
    rules.insert(
        8,
        Rule {
            id: 8,
            rule: RuleType::Or(vec![42], vec![42, 8]),
        },
    );
    rules.insert(
        11,
        Rule {
            id: 11,
            rule: RuleType::Or(vec![42, 31], vec![42, 11, 31]),
        },
    );
}

fn part2() {
    let (rules, messages) = read_input("input/day19.txt");
    let str_rules: Vec<&str> = rules.iter().map(|s| s.as_str()).collect();
    let mut parsed_rules = parse_rules(&str_rules);
    change_rules(&mut parsed_rules);

    let expr = format!("^{}$", build_regex(&parsed_rules, 0, 0));

    match Regex::new(expr.as_str()) {
        Ok(r) => {
            let valid = messages.iter().filter(|m| r.is_match(m)).count();
            println!("{}", valid);
        }
        Err(e) => {
            println!("{:?}", e);
        }
    };
}

fn main() {
    timing(part1, 19, 1);
    timing(part2, 19, 2);
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_DATA_1: &[&str] = &[
        "0: 4 1 5",
        "1: 2 3 | 3 2",
        "2: 4 4 | 5 5",
        "3: 4 5 | 5 4",
        "4: \"a\"",
        "5: \"b\"",
    ];

    #[test]
    fn test_part_1_example_1_parse() {
        let rule_0 = TEST_DATA_1[0]
            .parse::<Rule>()
            .expect("Could not parse rule 0.");
        assert_eq!(
            rule_0,
            Rule {
                id: 0,
                rule: RuleType::Seq(vec![4, 1, 5])
            }
        );

        let rule_1 = TEST_DATA_1[1]
            .parse::<Rule>()
            .expect("Could not parse rule 1.");
        assert_eq!(
            rule_1,
            Rule {
                id: 1,
                rule: RuleType::Or(vec![2, 3], vec!(3, 2))
            }
        );

        let rule_2 = TEST_DATA_1[2]
            .parse::<Rule>()
            .expect("Could not parse rule 1.");
        assert_eq!(
            rule_2,
            Rule {
                id: 2,
                rule: RuleType::Or(vec![4, 4], vec!(5, 5))
            }
        );

        let rule_3 = TEST_DATA_1[3]
            .parse::<Rule>()
            .expect("Could not parse rule 1.");
        assert_eq!(
            rule_3,
            Rule {
                id: 3,
                rule: RuleType::Or(vec![4, 5], vec!(5, 4))
            }
        );

        let rule_4 = TEST_DATA_1[4]
            .parse::<Rule>()
            .expect("Could not parse rule 1.");
        assert_eq!(
            rule_4,
            Rule {
                id: 4,
                rule: RuleType::Character('a')
            }
        );

        let rule_5 = TEST_DATA_1[5]
            .parse::<Rule>()
            .expect("Could not parse rule 1.");
        assert_eq!(
            rule_5,
            Rule {
                id: 5,
                rule: RuleType::Character('b')
            }
        );
    }

    #[test]
    fn test_part_2_example_1() {
        let rules = parse_rules(TEST_DATA_1);
        let expr = format!("^{}$", build_regex(&rules, 0, 0));
        let r = Regex::new(expr.as_str()).expect("Could not compile regex.");
        assert!(r.is_match("ababbb"));
        assert!(r.is_match("abbbab"));
        assert!(!r.is_match("bababa"));
        assert!(!r.is_match("aaabbb"));
        assert!(!r.is_match("aaaabbb"));
    }

    const TEST_DATA_2: &[&str] = &[
        "42: 9 14 | 10 1",
        "9: 14 27 | 1 26",
        "10: 23 14 | 28 1",
        "1: \"a\"",
        "11: 42 31",
        "5: 1 14 | 15 1",
        "19: 14 1 | 14 14",
        "12: 24 14 | 19 1",
        "16: 15 1 | 14 14",
        "31: 14 17 | 1 13",
        "6: 14 14 | 1 14",
        "2: 1 24 | 14 4",
        "0: 8 11",
        "13: 14 3 | 1 12",
        "15: 1 | 14",
        "17: 14 2 | 1 7",
        "23: 25 1 | 22 14",
        "28: 16 1",
        "4: 1 1",
        "20: 14 14 | 1 15",
        "3: 5 14 | 16 1",
        "27: 1 6 | 14 18",
        "14: \"b\"",
        "21: 14 1 | 1 14",
        "25: 1 1 | 1 14",
        "22: 14 14",
        "8: 42",
        "26: 14 22 | 1 20",
        "18: 15 15",
        "7: 14 5 | 1 21",
        "24: 14 1",
    ];

    const TEST_MSG_2: &[&str] = &[
        "abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa",
        "bbabbbbaabaabba",
        "babbbbaabbbbbabbbbbbaabaaabaaa",
        "aaabbbbbbaaaabaababaabababbabaaabbababababaaa",
        "bbbbbbbaaaabbbbaaabbabaaa",
        "bbbababbbbaaaaaaaabbababaaababaabab",
        "ababaaaaaabaaab",
        "ababaaaaabbbaba",
        "baabbaaaabbaaaababbaababb",
        "abbbbabbbbaaaababbbbbbaaaababb",
        "aaaaabbaabaaaaababaa",
        "aaaabbaaaabbaaa",
        "aaaabbaabbaaaaaaabbbabbbaaabbaabaaa",
        "babaaabbbaaabaababbaabababaaab",
        "aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba",
    ];

    #[test]
    fn test_part_2_example_2() {
        let mut rules = parse_rules(TEST_DATA_2);
        let expr = format!("^{}$", build_regex(&rules, 0, 0));
        let r = Regex::new(expr.as_str()).expect("Could not compile regex.");
        assert!(r.is_match("bbabbbbaabaabba"));
        assert!(r.is_match("ababaaaaaabaaab"));
        assert!(r.is_match("ababaaaaabbbaba"));
        let count_1 = TEST_MSG_2.iter().filter(|m| r.is_match(m)).count();
        assert_eq!(count_1, 3);

        change_rules(&mut rules);
        let expr2 = format!("^{}$", build_regex(&rules, 0, 0));
        let r2 = Regex::new(expr2.as_str()).expect("Could not compile regex.");
        let count_2 = TEST_MSG_2.iter().filter(|m| r2.is_match(m)).count();
        assert_eq!(count_2, 12);
    }
}
