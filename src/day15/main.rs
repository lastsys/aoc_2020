use common::performance::timing;
use std::collections::HashMap;

const INPUT_DATA: &[usize] = &[13, 16, 0, 12, 15, 1];

fn run(init: &[usize], last_turn: usize) -> usize {
    let mut seen = init[..(init.len() - 1)]
        .iter()
        .enumerate()
        .map(|(i, &e)| (e, i + 1))
        .collect::<HashMap<_, _>>();
    (init.len()..last_turn).fold(init[init.len() - 1], |last, i| {
        i - seen.insert(last, i).unwrap_or(i)
    })
}

fn part1() {
    let result = run(INPUT_DATA, 2020);
    println!("{}", result);
}

fn part2() {
    let result = run(INPUT_DATA, 30_000_000);
    println!("{}", result);
}

fn main() {
    timing(part1, 15, 1);
    timing(part2, 15, 2);
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_DATA_1: &[usize] = &[0, 3, 6]; // 436
    const TEST_DATA_2: &[usize] = &[1, 3, 2]; // 1
    const TEST_DATA_3: &[usize] = &[2, 1, 3]; // 10
    const TEST_DATA_4: &[usize] = &[1, 2, 3]; // 27
    const TEST_DATA_5: &[usize] = &[2, 3, 1]; // 78
    const TEST_DATA_6: &[usize] = &[3, 2, 1]; // 438
    const TEST_DATA_7: &[usize] = &[3, 1, 2]; // 1836

    #[test]
    fn test_example_1() {
        let val4 = run(TEST_DATA_1, 4);
        assert_eq!(val4, 0);

        let val5 = run(TEST_DATA_1, 5);
        assert_eq!(val5, 3);

        let val6 = run(TEST_DATA_1, 6);
        assert_eq!(val6, 3);

        let val7 = run(TEST_DATA_1, 7);
        assert_eq!(val7, 1);

        let val8 = run(TEST_DATA_1, 8);
        assert_eq!(val8, 0);

        let val9 = run(TEST_DATA_1, 9);
        assert_eq!(val9, 4);

        let val10 = run(TEST_DATA_1, 10);
        assert_eq!(val10, 0);
    }

    #[test]
    fn test_example_1_10_steps() {
        let result = run(TEST_DATA_1, 10);
        assert_eq!(result, 0);
    }

    #[test]
    fn test_example_1_2020_steps() {
        let result = run(TEST_DATA_1, 2020);
        assert_eq!(result, 436);
    }

    #[test]
    fn test_example_2_2020_steps() {
        let result = run(TEST_DATA_2, 2020);
        assert_eq!(result, 1);
    }

    #[test]
    fn test_example_3_2020_steps() {
        let result = run(TEST_DATA_3, 2020);
        assert_eq!(result, 10);
    }

    #[test]
    fn test_example_4_2020_steps() {
        let result = run(TEST_DATA_4, 2020);
        assert_eq!(result, 27);
    }

    #[test]
    fn test_example_5_2020_steps() {
        let result = run(TEST_DATA_5, 2020);
        assert_eq!(result, 78);
    }

    #[test]
    fn test_example_6_2020_steps() {
        let result = run(TEST_DATA_6, 2020);
        assert_eq!(result, 438);
    }

    #[test]
    fn test_example_7_2020_steps() {
        let result = run(TEST_DATA_7, 2020);
        assert_eq!(result, 1836);
    }
}
