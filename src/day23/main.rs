use common::performance::timing;

type CupId = usize;

const INPUT: &[CupId] = &[5, 8, 9, 1, 7, 4, 2, 6, 3];

fn show(cups: &[(usize, usize)]) -> String {
    let mut out = String::new();
    let mut i = 1;
    loop {
        i = cups[i].1;
        if i == 1 {
            break;
        }
        out.push_str(&format!("{}", i));
    }
    out
}

fn to_linked(cups: &[usize]) -> Vec<(usize, usize)> {
    let mut cups_linked: Vec<_> = vec![(0, 0); cups.len() + 1];
    // Initialize circular buffer.
    for window in cups.windows(3) {
        cups_linked[window[1]] = (window[0], window[2]);
    }
    // Edge cases.
    cups_linked[cups[0]] = (cups[cups.len() - 1], cups[1]);
    cups_linked[cups[cups.len() - 1]] = (cups[cups.len() - 2], cups[0]);
    cups_linked
}

fn step(cups: &mut Vec<(usize, usize)>, current_cup: &mut usize) {
    let mut destination_cup = if *current_cup == 1 {
        cups.len() - 1
    } else {
        *current_cup - 1
    };

    let next_1 = cups[*current_cup].1;
    let next_2 = cups[next_1].1;
    let next_3 = cups[next_2].1;

    while vec![next_1, next_2, next_3].contains(&destination_cup) {
        destination_cup = if destination_cup == 1 {
            cups.len() - 1
        } else {
            destination_cup - 1
        }
    }

    cups[*current_cup].1 = cups[next_3].1;

    let post_dest = cups[destination_cup].1;

    cups[destination_cup].1 = next_1;
    cups[next_1] = (destination_cup, next_2);
    cups[next_2] = (next_1, next_3);
    cups[next_3] = (next_2, post_dest);

    cups[post_dest].0 = next_3;

    *current_cup = cups[*current_cup].1;
}

fn part1() {
    let mut current_cup = INPUT[0];
    let mut cups = to_linked(INPUT);
    for _ in 0..100 {
        step(&mut cups, &mut current_cup);
    }
    println!("{}", show(&cups));
}

fn part2() {
    let mut input: Vec<_> = INPUT.to_vec();
    input.extend(
        input
            .iter()
            .copied()
            .max()
            .expect("Could not find maximum value.")
            + 1..=1_000_000,
    );

    let mut current_cup = input[0];
    let mut cups: Vec<_> = to_linked(&input);
    for _ in 0..10_000_000 {
        step(&mut cups, &mut current_cup);
    }
    let l1 = cups[1].1;
    let l2 = cups[l1].1;
    println!("{}", l1 * l2);
}

fn main() {
    timing(part1, 23, 1);
    timing(part2, 23, 2);
}
