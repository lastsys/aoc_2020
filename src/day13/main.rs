use common::performance::timing;
use num::Integer;
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::{BufRead, BufReader};

fn parse_data(input: &[String]) -> (usize, HashSet<usize>, Vec<(usize, usize)>) {
    let timestamp = input[0]
        .parse::<usize>()
        .expect("Could not parse timestamp.");
    let bus_ids: HashSet<usize> = input[1]
        .split(',')
        .filter(|s| *s != "x")
        .map(|s| s.parse::<usize>().expect("Could not parse bus id."))
        .collect();
    let bus_ids_2: Vec<(usize, usize)> = input[1]
        .split(',')
        .enumerate()
        .filter(|(_, s)| *s != "x")
        .map(|(pos, s)| (pos, s.parse::<usize>().expect("Could not parse bus id.")))
        .collect();
    (timestamp, bus_ids, bus_ids_2)
}

fn read_input(filename: &str) -> (usize, HashSet<usize>, Vec<(usize, usize)>) {
    let file = File::open(filename).expect("Could not open file.");
    let buffer = BufReader::new(file);
    let lines: Vec<String> = buffer
        .lines()
        .map(|s| s.expect("Could not read line."))
        .collect();
    parse_data(&lines)
}

fn find_next_departure(timestamp: usize, service: &HashSet<usize>) -> HashMap<usize, usize> {
    let mut next_departure = HashMap::new();
    for bus_id in service {
        let departure_count = timestamp / bus_id;
        let next = bus_id * (departure_count + 1);
        next_departure.insert(*bus_id, next);
    }
    next_departure
}

fn find_best_departure(departures: &HashMap<usize, usize>) -> (usize, usize) {
    departures.iter().fold(
        (0, usize::max_value()),
        |(best_bus_id, best_departure), (bus_id, departure)| {
            if *departure < best_departure {
                (*bus_id, *departure)
            } else {
                (best_bus_id, best_departure)
            }
        },
    )
}

fn part1() {
    let (timestamp, bus_ids, _) = read_input("input/day13.txt");
    let departures = find_next_departure(timestamp, &bus_ids);
    let (best_bus_id, best_departure) = find_best_departure(&departures);
    println!(
        "({} - {}) * {} = {}",
        best_departure,
        timestamp,
        best_bus_id,
        (best_departure - timestamp) * best_bus_id
    );
}

fn is_prime(number: usize) -> bool {
    if number % 2 == 0 {
        return false;
    }
    for n in (3..(number / 2)).step_by(2) {
        if number % n == 0 {
            return false;
        }
    }
    true
}

fn find_timestamp_with_close_departures(buses: &mut Vec<(usize, usize)>) -> usize {
    // Start by sorting the bus id:s.
    buses.sort_by(|(_, a), (_, b)| a.partial_cmp(b).expect("Failed to compare."));

    // Make sure all numbers are prime.
    let all_prime = buses.iter().all(|(_, v)| is_prime(*v));
    if !all_prime {
        panic!("All bus id:s are expected to be prime numbers.");
    }

    let mut step: usize = 1;
    let mut t: usize = 0;

    for (offset, bus_id) in buses {
        loop {
            if (t + *offset) % *bus_id == 0 {
                break;
            }
            t += step;
        }
        step = step.lcm(bus_id);
    }
    t
}

fn part2() {
    let (_, _, mut buses) = read_input("input/day13.txt");
    let timestamp = find_timestamp_with_close_departures(&mut buses);
    println!("{}", timestamp);
}

fn main() {
    timing(part1, 13, 1);
    timing(part2, 13, 2);
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_DATA: &[&str] = &["939", "7,13,x,x,59,x,31,19"];

    #[test]
    fn test_parse() {
        let input: Vec<String> = TEST_DATA.iter().map(|s| s.to_string()).collect();
        let (timestamp, bus_ids, _) = parse_data(&input);
        assert_eq!(timestamp, 939);
        assert_eq!(bus_ids.len(), 5);
        assert!(bus_ids.contains(&7));
        assert!(bus_ids.contains(&13));
        assert!(bus_ids.contains(&59));
        assert!(bus_ids.contains(&31));
        assert!(bus_ids.contains(&19));
    }

    #[test]
    fn test_next_departure() {
        let input: Vec<String> = TEST_DATA.iter().map(|s| s.to_string()).collect();
        let (timestamp, bus_ids, _) = parse_data(&input);
        let departures = find_next_departure(timestamp, &bus_ids);
        let best = find_best_departure(&departures);
        assert_eq!(best, (59, 944));
    }

    #[test]
    fn test_find_timestamp_with_close_departures() {
        let input: Vec<String> = TEST_DATA.iter().map(|s| s.to_string()).collect();
        let (_, _, mut buses) = parse_data(&input);
        let timestamp = find_timestamp_with_close_departures(&mut buses);
        assert_eq!(timestamp, 1068781);
    }
}
