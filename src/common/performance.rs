use std::time::Instant;

const WIDTH: usize = 75;

pub fn timing<A>(f: A, day: u32, part: u32)
where
    A: FnOnce(),
{
    let header = {
        let header_day = format!("Day {}", day);
        let header_part = format!("Part {}", part);
        format!(
            "-{}{}{}-",
            header_day,
            "-".repeat(WIDTH - header_day.len() - header_part.len() - 2),
            header_part
        )
    };
    assert_eq!(header.len(), WIDTH);
    println!("{}", header);

    let now = Instant::now();
    f();
    let elapsed = now.elapsed().as_micros();

    let footer = {
        let elapsed_string = format!("{} μs", elapsed);
        format!(
            "-{}{}-",
            "-".repeat(WIDTH - elapsed_string.len() - 1),
            elapsed_string
        )
    };
    assert_eq!(footer.len(), WIDTH + 1);
    println!("{}", footer);
}
