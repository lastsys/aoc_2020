use std::fs::File;
use std::io::{BufRead, BufReader, Error};
use std::result::Result;

/// Read input by folding line by line.
pub fn fold_input<A>(
    filename: &str,
    init: A,
    folder: fn(A, Result<String, Error>) -> A,
) -> std::io::Result<A> {
    let file = File::open(filename)?;
    let buffer = BufReader::new(file);
    let result = buffer.lines().fold(init, folder);
    Ok(result)
}
