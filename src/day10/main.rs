use common::input::fold_input;
use common::performance::timing;
use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::io::Error;

fn input_folder(container: &mut Vec<usize>, line: Result<String, Error>) -> &mut Vec<usize> {
    container.push(
        line.expect("Could not read line.")
            .parse()
            .expect("Could not parse number."),
    );
    container
}

fn diff(v: &[usize]) -> Vec<usize> {
    let mut diffs: Vec<usize> = Vec::new();
    // First.
    diffs.push(v[0]);
    // In-between.
    for i in 0..(v.len() - 1) {
        diffs.push(v[i + 1] - v[i]);
    }
    // Last.
    diffs.push(3);
    diffs
}

fn hist(values: &[usize]) -> HashMap<usize, usize> {
    let mut histogram: HashMap<usize, usize> = HashMap::new();
    for value in values {
        match histogram.entry(*value) {
            Entry::Vacant(e) => {
                e.insert(1);
            }
            Entry::Occupied(mut e) => {
                *e.get_mut() += 1;
            }
        }
    }
    histogram
}

fn measure_tree(all_values: &[usize]) -> usize {
    let mut memory: HashMap<usize, usize> = HashMap::new();

    fn walk(values: &[usize], memory: &mut HashMap<usize, usize>, index: usize) -> usize {
        let value = values[index];

        match memory.get(&index) {
            Some(precomp) => *precomp,
            None => {
                if index == (values.len() - 2) {
                    1
                } else {
                    let total = ((index + 1)..values.len())
                        .take_while(|i| (values[*i] - value) <= 3)
                        .map(|i| walk(values, memory, i))
                        .sum();
                    memory.insert(index, total);
                    total
                }
            }
        }
    }

    walk(all_values, &mut memory, 0)
}

fn part1() {
    let mut init: Vec<usize> = Vec::new();
    let input: &Vec<usize> =
        fold_input("input/day10.txt", &mut init, input_folder).expect("Could not read file.");
    let mut values = input.clone();
    values.sort_unstable();
    let d = diff(&values);
    let h = hist(&d);
    println!("{} * {} = {}", h[&1], h[&3], h[&1] * h[&3]);
}

fn part2() {
    let mut init: Vec<usize> = Vec::new();
    let input: &Vec<usize> =
        fold_input("input/day10.txt", &mut init, input_folder).expect("Could not read file.");
    let mut values = input.clone();
    values.sort_unstable();
    values.push(values[values.len() - 1] + 3);
    values.insert(0, 0);
    let size = measure_tree(&values);
    println!("{}", size);
}

fn main() {
    timing(part1, 10, 1);
    timing(part2, 10, 2);
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_DATA_1: &[usize] = &[16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4];

    #[test]
    fn test_example_1() {
        let mut test_data = Vec::from(TEST_DATA_1);
        test_data.sort_unstable();
        let d = diff(&test_data);
        let h = hist(&d);
        assert_eq!(h.len(), 2);
        assert_eq!(h[&1], 7);
        assert_eq!(h[&3], 5);
    }

    const TEST_DATA_2: &[usize] = &[
        28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35, 8,
        17, 7, 9, 4, 2, 34, 10, 3,
    ];

    #[test]
    fn test_example_2() {
        let mut test_data = Vec::from(TEST_DATA_2);
        test_data.sort_unstable();
        let d = diff(&test_data);
        let h = hist(&d);
        assert_eq!(h.len(), 2);
        assert_eq!(h[&1], 22);
        assert_eq!(h[&3], 10);
    }

    #[test]
    fn test_example_3() {
        let mut test_data = Vec::from(TEST_DATA_1);
        test_data.sort_unstable();
        test_data.push(test_data[test_data.len() - 1] + 3);
        test_data.insert(0, 0);
        let size = measure_tree(&test_data);
        assert_eq!(size, 8);
    }

    #[test]
    fn test_example_4() {
        let mut test_data = Vec::from(TEST_DATA_2);
        test_data.sort_unstable();
        test_data.push(test_data[test_data.len() - 1] + 3);
        test_data.insert(0, 0);
        let size = measure_tree(&test_data);
        assert_eq!(size, 19208);
    }
}
