use common::input::fold_input;
use common::performance::timing;
use std::io::Error;

type State = Vec<Vec<char>>;

struct Seats {
    pub state: State,
    pub width: usize,
    pub height: usize,
}

impl Seats {
    #[allow(clippy::ptr_arg)]
    pub fn new(initial_state: &State) -> Seats {
        let width = initial_state[0].len();
        let height = initial_state.len();
        Seats {
            state: initial_state.clone(),
            width,
            height,
        }
    }

    pub fn count_total(&self, symbol: char) -> usize {
        self.state.iter().fold(0, |count, row| {
            count
                + row.iter().fold(0, |inner_count, c| {
                    if *c == symbol {
                        inner_count + 1
                    } else {
                        inner_count
                    }
                })
        })
    }

    pub fn count_occupied_adjacent(&self, row: usize, col: usize) -> usize {
        const OFFSETS: [(i32, i32); 8] = [
            (-1, -1),
            (-1, 0),
            (-1, 1),
            (0, -1),
            (0, 1),
            (1, -1),
            (1, 0),
            (1, 1),
        ];

        let signed_row = row as i32;
        let signed_col = col as i32;

        OFFSETS.iter().fold(0, |count, (dy, dx)| {
            if ((row == 0) && (*dy == -1))
                || ((col == 0) && (*dx == -1))
                || ((row == (self.height - 1)) && (*dy == 1))
                || ((col == (self.width - 1)) && (*dx == 1))
            {
                count
            } else {
                let row_to_check = signed_row + dy;
                let col_to_check = signed_col + dx;
                match self.state[row_to_check as usize][col_to_check as usize] {
                    '#' => count + 1,
                    _ => count,
                }
            }
        })
    }

    pub fn count_occupied_visible(&self, row: usize, col: usize) -> usize {
        const DIRECTIONS: [(i32, i32); 8] = [
            (-1, -1),
            (-1, 0),
            (-1, 1),
            (0, -1),
            (0, 1),
            (1, -1),
            (1, 0),
            (1, 1),
        ];

        let signed_row = row as i32;
        let signed_col = col as i32;

        let mut count: usize = 0;
        for (dy, dx) in DIRECTIONS.iter() {
            let mut x = signed_col;
            let mut y = signed_row;
            loop {
                x += dx;
                y += dy;
                if (x < 0) || (x >= self.width as i32) || (y < 0) || (y >= self.height as i32) {
                    break;
                }
                match self.state[y as usize][x as usize] {
                    '#' => {
                        count += 1;
                        break;
                    }
                    'L' => {
                        break;
                    }
                    _ => (),
                }
            }
        }
        count
    }

    pub fn step(&mut self, visible_mode: bool) -> State {
        let old_state = self.state.clone();
        let mut updated_state = self.state.clone();
        #[allow(clippy::needless_range_loop)]
        for row in 0..self.height {
            for col in 0..self.width {
                updated_state[row][col] = match self.state[row][col] {
                    'L' if !visible_mode && self.count_occupied_adjacent(row, col) == 0 => '#',
                    '#' if !visible_mode && self.count_occupied_adjacent(row, col) >= 4 => 'L',
                    'L' if visible_mode && self.count_occupied_visible(row, col) == 0 => '#',
                    '#' if visible_mode && self.count_occupied_visible(row, col) >= 5 => 'L',
                    _ => self.state[row][col],
                }
            }
        }
        self.state = updated_state;
        old_state
    }
}

fn input_folder(container: &mut State, line: Result<String, Error>) -> &mut State {
    container.push(line.expect("Could not read line.").trim().chars().collect());
    container
}

// fn print_state(state: Vec<Vec<char>>) {
//     for col in 0..state[0].len() {
//         for row in 0..state.len() {
//             print!("{}", state[row][col]);
//         }
//         println!();
//     }
// }

fn part1() {
    let mut init: State = Vec::new();
    let lines: &State =
        fold_input("input/day11.txt", &mut init, input_folder).expect("Could not read file.");
    let mut seats = Seats::new(lines);
    loop {
        let old_state = seats.step(false);
        if old_state == seats.state {
            break;
        }
    }
    println!("{}", seats.count_total('#'));
}

fn part2() {
    let mut init: State = Vec::new();
    let lines: &State =
        fold_input("input/day11.txt", &mut init, input_folder).expect("Could not read file.");
    let mut seats = Seats::new(lines);
    loop {
        let old_state = seats.step(true);
        if old_state == seats.state {
            break;
        }
    }
    println!("{}", seats.count_total('#'));
}

fn main() {
    timing(part1, 11, 1);
    timing(part2, 11, 2);
}

#[cfg(test)]
mod test {
    use super::*;

    const PART_1_STEP_0: &[&str] = &[
        "L.LL.LL.LL",
        "LLLLLLL.LL",
        "L.L.L..L..",
        "LLLL.LL.LL",
        "L.LL.LL.LL",
        "L.LLLLL.LL",
        "..L.L.....",
        "LLLLLLLLLL",
        "L.LLLLLL.L",
        "L.LLLLL.LL",
    ];

    const PART_1_STEP_1: &[&str] = &[
        "#.##.##.##",
        "#######.##",
        "#.#.#..#..",
        "####.##.##",
        "#.##.##.##",
        "#.#####.##",
        "..#.#.....",
        "##########",
        "#.######.#",
        "#.#####.##",
    ];

    const PART_1_STEP_2: &[&str] = &[
        "#.LL.L#.##",
        "#LLLLLL.L#",
        "L.L.L..L..",
        "#LLL.LL.L#",
        "#.LL.LL.LL",
        "#.LLLL#.##",
        "..L.L.....",
        "#LLLLLLLL#",
        "#.LLLLLL.L",
        "#.#LLLL.##",
    ];

    const PART_1_STEP_3: &[&str] = &[
        "#.##.L#.##",
        "#L###LL.L#",
        "L.#.#..#..",
        "#L##.##.L#",
        "#.##.LL.LL",
        "#.###L#.##",
        "..#.#.....",
        "#L######L#",
        "#.LL###L.L",
        "#.#L###.##",
    ];

    const PART_1_STEP_4: &[&str] = &[
        "#.#L.L#.##",
        "#LLL#LL.L#",
        "L.L.L..#..",
        "#LLL.##.L#",
        "#.LL.LL.LL",
        "#.LL#L#.##",
        "..L.L.....",
        "#L#LLLL#L#",
        "#.LLLLLL.L",
        "#.#L#L#.##",
    ];

    const PART_1_STEP_5: &[&str] = &[
        "#.#L.L#.##",
        "#LLL#LL.L#",
        "L.#.L..#..",
        "#L##.##.L#",
        "#.#L.LL.LL",
        "#.#L#L#.##",
        "..L.L.....",
        "#L#L##L#L#",
        "#.LLLLLL.L",
        "#.#L#L#.##",
    ];

    fn const_to_vec(const_seats: &[&str]) -> State {
        Vec::from(const_seats)
            .iter()
            .map(|s| s.chars().collect())
            .collect()
    }

    #[test]
    fn test_example_1() {
        let step_0 = const_to_vec(PART_1_STEP_0);
        let step_1 = const_to_vec(PART_1_STEP_1);
        let step_2 = const_to_vec(PART_1_STEP_2);
        let step_3 = const_to_vec(PART_1_STEP_3);
        let step_4 = const_to_vec(PART_1_STEP_4);
        let step_5 = const_to_vec(PART_1_STEP_5);

        let mut seats = Seats::new(&step_0);
        seats.step(false);
        assert_eq!(seats.state, step_1);
        seats.step(false);
        assert_eq!(seats.state, step_2);
        seats.step(false);
        assert_eq!(seats.state, step_3);
        seats.step(false);
        assert_eq!(seats.state, step_4);
        seats.step(false);
        assert_eq!(seats.state, step_5);
        seats.step(false);
        assert_eq!(seats.state, step_5);
        assert_eq!(seats.count_total('#'), 37);
    }

    const PART_2_EXAMPLE_1: &[&str] = &[
        ".......#.",
        "...#.....",
        ".#.......",
        ".........",
        "..#L....#",
        "....#....",
        ".........",
        "#........",
        "...#.....",
    ];

    const PART_2_EXAMPLE_2: &[&str] = &[".............", ".L.L.#.#.#.#.", "............."];

    const PART_2_EXAMPLE_3: &[&str] = &[
        ".##.##.", "#.#.#.#", "##...##", "...L...", "##...##", "#.#.#.#", ".##.##.",
    ];

    const PART_2_STEP_0: &[&str] = &[
        "L.LL.LL.LL",
        "LLLLLLL.LL",
        "L.L.L..L..",
        "LLLL.LL.LL",
        "L.LL.LL.LL",
        "L.LLLLL.LL",
        "..L.L.....",
        "LLLLLLLLLL",
        "L.LLLLLL.L",
        "L.LLLLL.LL",
    ];

    const PART_2_STEP_1: &[&str] = &[
        "#.##.##.##",
        "#######.##",
        "#.#.#..#..",
        "####.##.##",
        "#.##.##.##",
        "#.#####.##",
        "..#.#.....",
        "##########",
        "#.######.#",
        "#.#####.##",
    ];

    const PART_2_STEP_2: &[&str] = &[
        "#.LL.LL.L#",
        "#LLLLLL.LL",
        "L.L.L..L..",
        "LLLL.LL.LL",
        "L.LL.LL.LL",
        "L.LLLLL.LL",
        "..L.L.....",
        "LLLLLLLLL#",
        "#.LLLLLL.L",
        "#.LLLLL.L#",
    ];

    const PART_2_STEP_3: &[&str] = &[
        "#.L#.##.L#",
        "#L#####.LL",
        "L.#.#..#..",
        "##L#.##.##",
        "#.##.#L.##",
        "#.#####.#L",
        "..#.#.....",
        "LLL####LL#",
        "#.L#####.L",
        "#.L####.L#",
    ];

    const PART_2_STEP_4: &[&str] = &[
        "#.L#.L#.L#",
        "#LLLLLL.LL",
        "L.L.L..#..",
        "##LL.LL.L#",
        "L.LL.LL.L#",
        "#.LLLLL.LL",
        "..L.L.....",
        "LLLLLLLLL#",
        "#.LLLLL#.L",
        "#.L#LL#.L#",
    ];

    const PART_2_STEP_5: &[&str] = &[
        "#.L#.L#.L#",
        "#LLLLLL.LL",
        "L.L.L..#..",
        "##L#.#L.L#",
        "L.L#.#L.L#",
        "#.L####.LL",
        "..#.#.....",
        "LLL###LLL#",
        "#.LLLLL#.L",
        "#.L#LL#.L#",
    ];

    const PART_2_STEP_6: &[&str] = &[
        "#.L#.L#.L#",
        "#LLLLLL.LL",
        "L.L.L..#..",
        "##L#.#L.L#",
        "L.L#.LL.L#",
        "#.LLLL#.LL",
        "..#.L.....",
        "LLL###LLL#",
        "#.LLLLL#.L",
        "#.L#LL#.L#",
    ];

    #[test]
    fn test_part_2_example_1() {
        let example = const_to_vec(PART_2_EXAMPLE_1);
        let seats = Seats::new(&example);
        let visible = seats.count_occupied_visible(4, 3);
        assert_eq!(visible, 8);
    }

    #[test]
    fn test_part_2_example_2() {
        let example = const_to_vec(PART_2_EXAMPLE_2);
        let seats = Seats::new(&example);
        let visible = seats.count_occupied_visible(1, 1);
        assert_eq!(visible, 0);
    }

    #[test]
    fn test_part_2_example_3() {
        let example = const_to_vec(PART_2_EXAMPLE_3);
        let seats = Seats::new(&example);
        let visible = seats.count_occupied_visible(3, 3);
        assert_eq!(visible, 0);
    }

    #[test]
    fn test_part_2_steps() {
        let step_0 = const_to_vec(PART_2_STEP_0);
        let step_1 = const_to_vec(PART_2_STEP_1);
        let step_2 = const_to_vec(PART_2_STEP_2);
        let step_3 = const_to_vec(PART_2_STEP_3);
        let step_4 = const_to_vec(PART_2_STEP_4);
        let step_5 = const_to_vec(PART_2_STEP_5);
        let step_6 = const_to_vec(PART_2_STEP_6);

        let mut seats = Seats::new(&step_0);
        seats.step(true);
        assert_eq!(seats.state, step_1);
        seats.step(true);
        assert_eq!(seats.state, step_2);
        seats.step(true);
        assert_eq!(seats.state, step_3);
        seats.step(true);
        assert_eq!(seats.state, step_4);
        seats.step(true);
        assert_eq!(seats.state, step_5);
        seats.step(true);
        assert_eq!(seats.state, step_6);
        seats.step(true);
        assert_eq!(seats.state, step_6);
        assert_eq!(seats.count_total('#'), 26);
    }
}
