use common::input::fold_input;
use common::performance::timing;
use std::io::Error;

struct Position {
    x: usize,
    y: usize,
}

impl Position {
    pub fn go(self: &mut Position, dx: usize, dy: usize) {
        self.x += dx;
        self.y += dy;
    }
}

fn input_folder(
    container: &mut Vec<Vec<char>>,
    line: Result<String, Error>,
) -> &mut Vec<Vec<char>> {
    let current_line: Vec<char> = line.expect("Failed to read line.").chars().collect();
    container.push(current_line);
    container
}

fn count_trees(map: &[Vec<char>], dx: usize, dy: usize) -> usize {
    let mut position = Position { x: 0, y: 0 };
    let height = map.len();
    let width = map[0].len();
    let mut tree_count: usize = 0;
    while (position.y as usize) < height {
        let map_x = position.x % width;
        if map[position.y][map_x] == '#' {
            tree_count += 1;
        }
        position.go(dx, dy);
    }
    tree_count
}

fn part1() {
    let mut init: Vec<Vec<char>> = Vec::new();
    let input =
        fold_input("input/day03.txt", &mut init, input_folder).expect("Failed to read input.");
    let tree_count = count_trees(input, 3, 1);
    println!("Tree Count = {}", tree_count);
}

fn try_slopes(map: &[Vec<char>]) -> Vec<usize> {
    let slopes: [(usize, usize); 5] = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
    slopes
        .iter()
        .map(|(dx, dy)| count_trees(map, *dx, *dy))
        .collect()
}

fn part2() {
    let mut init: Vec<Vec<char>> = Vec::new();
    let input =
        fold_input("input/day03.txt", &mut init, input_folder).expect("Failed to read input.");
    let tree_product: usize = try_slopes(input).iter().product();
    println!("Tree product = {}", tree_product);
}

fn main() {
    timing(part1, 3, 1);
    timing(part2, 3, 2);
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &'static [&str] = &[
        "..##.......",
        "#...#...#..",
        ".#....#..#.",
        "..#.#...#.#",
        ".#...##..#.",
        "..#.##.....",
        ".#.#.#....#",
        ".#........#",
        "#.##...#...",
        "#...##....#",
        ".#..#...#.#",
    ];

    #[test]
    fn test_count_trees() {
        let mut input: Vec<Vec<char>> = Vec::new();
        for row in TEST_INPUT {
            input.push(row.chars().collect());
        }
        let tree_count = count_trees(&input, 3, 1);
        assert_eq!(tree_count, 7);
    }

    #[test]
    fn test_try_slopes() {
        let mut input: Vec<Vec<char>> = Vec::new();
        for row in TEST_INPUT {
            input.push(row.chars().collect());
        }
        let tree_counts = try_slopes(&input);
        assert_eq!(tree_counts[0], 2);
        assert_eq!(tree_counts[1], 7);
        assert_eq!(tree_counts[2], 3);
        assert_eq!(tree_counts[3], 4);
        assert_eq!(tree_counts[4], 2)
    }
}
