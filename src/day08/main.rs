use common::input::fold_input;
use common::performance::timing;
use std::collections::HashSet;
use std::io::Error;

#[derive(PartialEq, Debug, Clone)]
enum Instruction {
    Acc(i32),
    Jmp(i32),
    Nop(i32),
}

struct Machine<'a> {
    pub pc: i32,
    pub acc: i32,
    pub program: &'a [Instruction],
}

impl Machine<'_> {
    pub fn new(program: &[Instruction]) -> Machine {
        Machine {
            pc: 0,
            acc: 0,
            program,
        }
    }

    pub fn step(&mut self) {
        match self.program[self.pc as usize] {
            Instruction::Acc(arg) => {
                self.acc += arg;
                self.pc += 1;
            }
            Instruction::Jmp(arg) => {
                self.pc += arg;
            }
            Instruction::Nop(_) => {
                self.pc += 1;
            }
        }
        assert!(self.pc >= 0);
    }
}

fn input_folder(container: &mut Vec<String>, line: Result<String, Error>) -> &mut Vec<String> {
    container.push(line.expect("Could not read line.").trim().to_string());
    container
}

fn parse_program(source: &[&str]) -> Vec<Instruction> {
    source.iter().fold(Vec::new(), |mut program, line| {
        let parts: Vec<&str> = line.split_whitespace().collect();
        let command = parts[0];
        let argument: i32 = parts[1].parse().expect("Could not parse argument.");
        let instruction = match (command, argument) {
            ("nop", arg) => Instruction::Nop(arg),
            ("acc", arg) => Instruction::Acc(arg),
            ("jmp", arg) => Instruction::Jmp(arg),
            _ => panic!("Invalid instruction."),
        };
        program.push(instruction);
        program
    })
}

fn run_program(program: &[Instruction]) -> Machine {
    let mut visited: HashSet<i32> = HashSet::new();
    let mut machine = Machine::new(&program);
    while !visited.contains(&machine.pc) {
        visited.insert(machine.pc);
        machine.step();
        // End of program.
        if machine.pc as usize == program.len() {
            break;
        }
    }
    machine
}

fn part1() {
    let mut init: Vec<String> = Vec::new();
    let source: &Vec<String> =
        fold_input("input/day08.txt", &mut init, input_folder).expect("Could not read file.");
    let str_source: Vec<&str> = source.iter().map(|s| s.as_str()).collect();
    let program = parse_program(&str_source);
    let machine = run_program(&program);
    println!("acc = {}", machine.acc);
}

fn part2() {
    let mut init: Vec<String> = Vec::new();
    let source: &Vec<String> =
        fold_input("input/day08.txt", &mut init, input_folder).expect("Could not read file.");
    let str_source: Vec<&str> = source.iter().map(|s| s.as_str()).collect();
    let program = parse_program(&str_source);

    for i in 0..program.len() {
        let mut candidate_program = program.clone();
        match candidate_program[i] {
            Instruction::Jmp(arg) => candidate_program[i] = Instruction::Nop(arg),
            Instruction::Nop(arg) => candidate_program[i] = Instruction::Jmp(arg),
            _ => continue,
        }
        let machine = run_program(&candidate_program);
        if machine.pc as usize == candidate_program.len() {
            println!("acc = {}", machine.acc);
            break;
        }
    }
}

fn main() {
    timing(part1, 8, 1);
    timing(part2, 8, 2);
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_PROGRAM: &[&str] = &[
        "nop +0", "acc +1", "jmp +4", "acc +3", "jmp -3", "acc -99", "acc +1", "jmp -4", "acc +6",
    ];

    #[test]
    fn test_program_parser() {
        let program = parse_program(&Vec::from(TEST_PROGRAM));
        assert_eq!(program.len(), TEST_PROGRAM.len());
        assert_eq!(program[0], Instruction::Nop(0));
        assert_eq!(program[1], Instruction::Acc(1));
        assert_eq!(program[2], Instruction::Jmp(4));
        assert_eq!(program[3], Instruction::Acc(3));
        assert_eq!(program[4], Instruction::Jmp(-3));
        assert_eq!(program[5], Instruction::Acc(-99));
        assert_eq!(program[6], Instruction::Acc(1));
        assert_eq!(program[7], Instruction::Jmp(-4));
        assert_eq!(program[8], Instruction::Acc(6));
    }

    #[test]
    fn test_machine() {
        let program = parse_program(&Vec::from(TEST_PROGRAM));
        let mut machine = Machine::new(&program);
        assert_eq!(machine.pc, 0);
        assert_eq!(machine.acc, 0);
        machine.step();
        assert_eq!(machine.pc, 1);
        assert_eq!(machine.acc, 0);
        machine.step();
        assert_eq!(machine.pc, 2);
        assert_eq!(machine.acc, 1);
        machine.step();
        assert_eq!(machine.pc, 6);
        assert_eq!(machine.acc, 1);
        machine.step();
        assert_eq!(machine.pc, 7);
        assert_eq!(machine.acc, 2);
        machine.step();
        assert_eq!(machine.pc, 3);
        assert_eq!(machine.acc, 2);
        machine.step();
        assert_eq!(machine.pc, 4);
        assert_eq!(machine.acc, 5);
        machine.step();
        assert_eq!(machine.pc, 1);
        assert_eq!(machine.acc, 5);
    }
}
