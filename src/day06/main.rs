use common::input::fold_input;
use common::performance::timing;
use std::collections::HashSet;
use std::io::Error;

fn input_folder(container: &mut Vec<String>, line: Result<String, Error>) -> &mut Vec<String> {
    container.push(line.expect("Could not read line.").trim().to_string());
    container
}

fn parse_answers(input: &[String]) -> Vec<HashSet<char>> {
    let groups: Vec<String> = input.split(|s| s.is_empty()).map(|s| s.join("")).collect();
    let mut group_answers: Vec<HashSet<char>> = Vec::new();
    for group in groups {
        let answers = group.chars().fold(HashSet::new(), |mut set, c| {
            set.insert(c);
            set
        });
        group_answers.push(answers);
    }
    group_answers
}

fn parse_group(answers: &[String]) -> Vec<HashSet<char>> {
    answers
        .iter()
        .map(|individual| {
            individual.chars().fold(HashSet::new(), |mut set, c| {
                set.insert(c);
                set
            })
        })
        .collect()
}

fn merge_group(group_answers: Vec<HashSet<char>>) -> HashSet<char> {
    let first_set = group_answers[0].clone();
    group_answers[1..].iter().fold(first_set, |set, group| {
        set.intersection(group).cloned().collect()
    })
}

fn parse_answers_2(input: &[String]) -> Vec<HashSet<char>> {
    let groups: Vec<&[String]> = input.split(|s| s.is_empty()).collect();
    let mut group_intersection_answers: Vec<HashSet<char>> = Vec::new();
    for group in groups {
        let parsed = parse_group(group);
        let merged = merge_group(parsed);
        group_intersection_answers.push(merged);
    }
    group_intersection_answers
}

fn part1() {
    let mut init: Vec<String> = Vec::new();
    let lines =
        fold_input("input/day06.txt", &mut init, input_folder).expect("Could not read file.");
    let answers = parse_answers(lines);
    let count: usize = answers.iter().map(|set| set.len()).sum();
    println!("Answer count sum = {}", count);
}

fn part2() {
    let mut init: Vec<String> = Vec::new();
    let lines =
        fold_input("input/day06.txt", &mut init, input_folder).expect("Could not read file.");
    let answers = parse_answers_2(lines);
    let count: usize = answers.iter().map(|set| set.len()).sum();
    println!("Answer count sum = {}", count);
}

fn main() {
    timing(part1, 6, 1);
    timing(part2, 6, 2);
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_DATA: &[&str] = &[
        "abc", "", "a", "b", "c", "", "ab", "ac", "", "a", "a", "a", "a", "", "b",
    ];

    #[test]
    fn test_parse_answers() {
        let test_data: Vec<String> = TEST_DATA.iter().map(|s| s.to_string()).collect();
        let answers = parse_answers(&test_data);
        assert_eq!(answers.len(), 5);
        assert_eq!(answers[0].len(), 3);
        assert_eq!(answers[1].len(), 3);
        assert_eq!(answers[2].len(), 3);
        assert_eq!(answers[3].len(), 1);
        assert_eq!(answers[4].len(), 1);
        let count: usize = answers.iter().map(|set| set.len()).sum();
        assert_eq!(count, 11);
    }

    #[test]
    fn test_parse_answers_2() {
        let test_data: Vec<String> = TEST_DATA.iter().map(|s| s.to_string()).collect();
        let answers = parse_answers_2(&test_data);
        assert_eq!(answers.len(), 5);
        assert_eq!(answers[0].len(), 3);
        assert_eq!(answers[1].len(), 0);
        assert_eq!(answers[2].len(), 1);
        assert_eq!(answers[3].len(), 1);
        assert_eq!(answers[4].len(), 1);
        let count: usize = answers.iter().map(|set| set.len()).sum();
        assert_eq!(count, 6);
    }
}
