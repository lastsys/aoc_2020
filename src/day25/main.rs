use common::performance::timing;
use std::fs::File;
use std::io::{BufRead, BufReader};

const SUBJECT: usize = 7;
const DIVIDER: usize = 20201227;

fn calc_encryption(public_key: usize, loop_size: usize) -> usize {
    let mut value: usize = 1;
    for _ in 0..loop_size {
        value = value * public_key % DIVIDER;
    }
    value
}

fn find_loop_size(public_key: usize) -> usize {
    let mut value = 1;
    let mut loop_size: usize = 0;
    while value != public_key {
        value = value * SUBJECT % DIVIDER;
        loop_size += 1;
    }
    loop_size
}

fn read_data(filename: &str) -> (usize, usize) {
    let file = File::open(filename).expect("Could not find file.");
    let buffer = BufReader::new(file);
    let mut lines = buffer.lines();
    let line_1 = lines.next().unwrap().unwrap().parse::<usize>().unwrap();
    let line_2 = lines.next().unwrap().unwrap().parse::<usize>().unwrap();
    (line_1, line_2)
}

fn part1() {
    let (card_pub_key, door_pub_key) = read_data("input/day25.txt");
    let door_loop_size = find_loop_size(door_pub_key);
    let card_priv_key = calc_encryption(card_pub_key, door_loop_size);
    println!("{}", card_priv_key);
}

fn main() {
    timing(part1, 25, 1);
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part_1_example_1() {
        const CARD_PUBLIC_KEY: usize = 5764801;
        const DOOR_PUBLIC_KEY: usize = 17807724;

        let card_loop_size = find_loop_size(CARD_PUBLIC_KEY);
        assert_eq!(card_loop_size, 8);
        let door_loop_size = find_loop_size(DOOR_PUBLIC_KEY);
        assert_eq!(door_loop_size, 11);

        let card_key = calc_encryption(CARD_PUBLIC_KEY, door_loop_size);
        assert_eq!(card_key, 14897079);
        let door_key = calc_encryption(DOOR_PUBLIC_KEY, card_loop_size);
        assert_eq!(door_key, 14897079);
    }
}
