use common::input::fold_input;
use common::performance::timing;
use std::cmp::max;
use std::io::Error;

type SeatRanges = ((i32, i32), (i32, i32));

fn input_folder(container: &mut Vec<String>, line: Result<String, Error>) -> &mut Vec<String> {
    container.push(line.expect("Could not read line.").trim().to_string());
    container
}

fn parse_boarding_pass(pass: &str) -> (i32, i32) {
    let seat: SeatRanges = pass.chars().fold(((0, 127), (0, 7)), parse_instruction);
    (seat.0 .0, seat.1 .0)
}

fn parse_instruction(
    ((row_low, row_high), (col_low, col_high)): SeatRanges,
    instruction: char,
) -> SeatRanges {
    match instruction {
        'B' => {
            let row_delta = row_high - row_low + 1;
            ((row_low + row_delta / 2, row_high), (col_low, col_high))
        }
        'F' => {
            let row_delta = row_high - row_low + 1;
            ((row_low, row_high - row_delta / 2), (col_low, col_high))
        }
        'R' => {
            let col_delta = col_high - col_low + 1;
            ((row_low, row_high), (col_low + col_delta / 2, col_high))
        }
        'L' => {
            let col_delta = col_high - col_low + 1;
            ((row_low, row_high), (col_low, col_high - col_delta / 2))
        }
        _ => panic!("Invalid instruction."),
    }
}

fn find_max_seat_id(lines: &[String]) -> i32 {
    lines
        .iter()
        .map(|line| parse_boarding_pass(line))
        .map(|(row, col)| row * 8 + col)
        .fold(i32::min_value(), max)
}

fn find_missing_seat_id(lines: &[String]) -> i32 {
    let mut seat_ids: Vec<i32> = lines
        .iter()
        .map(|line| parse_boarding_pass(line))
        .map(|(row, col)| row * 8 + col)
        .collect();
    seat_ids.sort_unstable();
    for i in 1..seat_ids.len() {
        if seat_ids[i] - seat_ids[i - 1] > 1 {
            return seat_ids[i] - 1;
        }
    }
    panic!("Could not find the missing seat.");
}

fn part1() {
    let mut init: Vec<String> = Vec::new();
    let lines: &Vec<String> =
        fold_input("input/day05.txt", &mut init, input_folder).expect("Could not read file.");
    let max_id = find_max_seat_id(lines);
    println!("Maximum Seat ID = {}", max_id);
}

fn part2() {
    let mut init: Vec<String> = Vec::new();
    let lines: &Vec<String> =
        fold_input("input/day05.txt", &mut init, input_folder).expect("Could not read file.");
    let your_seat_id = find_missing_seat_id(lines);
    println!("Your seat ID = {}", your_seat_id);
}

fn main() {
    timing(part1, 5, 1);
    timing(part2, 5, 2);
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_DATA: &[&str] = &["FBFBBFFRLR", "BFFFBBFRRR", "FFFBBBFRRR", "BBFFBBFRLL"];

    #[test]
    fn test_parse_instruction() {
        let instructions: Vec<char> = TEST_DATA[0].chars().collect();
        let step_0 = ((0, 127), (0, 7));
        let step_1 = parse_instruction(step_0, instructions[0]);
        assert_eq!(step_1, ((0, 63), (0, 7)));
        let step_2 = parse_instruction(step_1, instructions[1]);
        assert_eq!(step_2, ((32, 63), (0, 7)));
        let step_3 = parse_instruction(step_2, instructions[2]);
        assert_eq!(step_3, ((32, 47), (0, 7)));
        let step_4 = parse_instruction(step_3, instructions[3]);
        assert_eq!(step_4, ((40, 47), (0, 7)));
        let step_5 = parse_instruction(step_4, instructions[4]);
        assert_eq!(step_5, ((44, 47), (0, 7)));
        let step_6 = parse_instruction(step_5, instructions[5]);
        assert_eq!(step_6, ((44, 45), (0, 7)));
        let step_7 = parse_instruction(step_6, instructions[6]);
        assert_eq!(step_7, ((44, 44), (0, 7)));

        let step_8 = parse_instruction(step_7, instructions[7]);
        assert_eq!(step_8, ((44, 44), (4, 7)));
        let step_9 = parse_instruction(step_8, instructions[8]);
        assert_eq!(step_9, ((44, 44), (4, 5)));
        let step_10 = parse_instruction(step_9, instructions[9]);
        assert_eq!(step_10, ((44, 44), (5, 5)));
    }

    #[test]
    fn test_parse_boarding_pass() {
        let pass_0 = parse_boarding_pass(TEST_DATA[0]);
        assert_eq!(pass_0, (44, 5));
        assert_eq!(pass_0.0 * 8 + pass_0.1, 357);
        let pass_1 = parse_boarding_pass(TEST_DATA[1]);
        assert_eq!(pass_1, (70, 7));
        assert_eq!(pass_1.0 * 8 + pass_1.1, 567);
        let pass_2 = parse_boarding_pass(TEST_DATA[2]);
        assert_eq!(pass_2, (14, 7));
        assert_eq!(pass_2.0 * 8 + pass_2.1, 119);
        let pass_3 = parse_boarding_pass(TEST_DATA[3]);
        assert_eq!(pass_3, (102, 4));
        assert_eq!(pass_3.0 * 8 + pass_3.1, 820);
    }

    #[test]
    fn test_find_max_seat_id() {
        let test_data: Vec<String> = TEST_DATA.iter().map(|line| line.to_string()).collect();
        let seat_id = find_max_seat_id(&test_data);
        assert_eq!(seat_id, 820);
    }
}
