use common::performance::timing;
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::iter::FromIterator;
use std::str::FromStr;

#[derive(Debug)]
struct Food {
    pub ingredients: Vec<String>,
    pub allergens: Vec<String>,
}

impl FromStr for Food {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = s.split(" (contains ").collect();
        let ingredients: Vec<String> = parts[0].split_whitespace().map(|s| s.to_string()).collect();
        let allergens: Vec<String> = parts[1]
            .split(')')
            .next()
            .expect("Could not get allergens.")
            .split(", ")
            .map(|s| s.to_string())
            .collect();
        Ok(Food {
            ingredients,
            allergens,
        })
    }
}

fn count_good(foods: &[Food]) -> usize {
    let mut all_ingredients: HashSet<String> = HashSet::new();
    let mut ingredient_count: HashMap<String, usize> = HashMap::new();
    let mut allergens: HashMap<String, HashSet<String>> = HashMap::new();
    for food in foods {
        let ingredient_set = HashSet::from_iter(food.ingredients.iter().cloned());
        all_ingredients = all_ingredients.union(&ingredient_set).cloned().collect();
        for ingredient in food.ingredients.iter() {
            ingredient_count
                .entry(ingredient.to_owned())
                .and_modify(|e| {
                    *e += 1;
                })
                .or_insert(1);
        }
        for allergen in food.allergens.iter() {
            allergens
                .entry(allergen.to_owned())
                .and_modify(|e| {
                    *e = e.intersection(&ingredient_set).cloned().collect();
                })
                .or_insert_with(|| ingredient_set.iter().cloned().collect());
        }
    }

    let bad_food = allergens.values().fold(HashSet::new(), |set, ingredients| {
        set.union(ingredients).cloned().collect()
    });

    let count: usize = all_ingredients
        .difference(&bad_food)
        .map(|i| ingredient_count[i])
        .sum();

    count
}

fn find_dangerous(foods: &[Food]) -> Vec<String> {
    let mut allergens: HashMap<String, HashSet<String>> = HashMap::new();
    for food in foods.iter() {
        let ingredient_set = HashSet::from_iter(food.ingredients.iter().cloned());
        for allergen in food.allergens.iter() {
            allergens
                .entry(allergen.to_owned())
                .and_modify(|e| {
                    *e = e.intersection(&ingredient_set).cloned().collect();
                })
                .or_insert_with(|| ingredient_set.iter().cloned().collect());
        }
    }

    let mut taken = HashSet::new();
    let mut items: Vec<(String, String)> = Vec::new();
    loop {
        let mut done = true;
        for (allergen, ingredients) in allergens.iter() {
            let diff = Vec::from_iter(ingredients.difference(&taken).cloned());
            if diff.len() == 1 {
                items.push((allergen.clone(), diff[0].clone()));
                taken.insert(diff[0].clone());
                done = false;
                break;
            }
        }
        if done {
            break;
        }
    }

    items.sort_by(|(a, _), (b, _)| a.partial_cmp(b).expect("Could not compare."));
    items.iter().map(|(_, i)| i).cloned().collect()
}

fn read_input(filename: &str) -> Vec<Food> {
    let file = File::open(filename).expect("Could not find file.");
    let buffer = BufReader::new(file);
    buffer
        .lines()
        .map(|s| {
            s.expect("Could not read line.")
                .parse::<Food>()
                .expect("Could not parse food.")
        })
        .collect()
}

fn part1() {
    let foods = read_input("input/day21.txt");
    let count = count_good(&foods);
    println!("{}", count);
}

fn part2() {
    let foods = read_input("input/day21.txt");
    let dangerous = find_dangerous(&foods);
    println!("{}", dangerous.join(","));
}

fn main() {
    timing(part1, 21, 1);
    timing(part2, 21, 2);
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_INPUT_1: &[&str] = &[
        "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)",
        "trh fvjkl sbzzf mxmxvkd (contains dairy)",
        "sqjhc fvjkl (contains soy)",
        "sqjhc mxmxvkd sbzzf (contains fish)",
    ];

    #[test]
    fn test_parse() {
        let food_0 = TEST_INPUT_1[0]
            .parse::<Food>()
            .expect("Could not parse food 0.");
        assert_eq!(
            food_0.ingredients,
            vec!["mxmxvkd", "kfcds", "sqjhc", "nhms"]
        );
        assert_eq!(food_0.allergens, vec!["dairy", "fish"]);

        let food_1 = TEST_INPUT_1[1]
            .parse::<Food>()
            .expect("Could not parse food 1.");
        assert_eq!(food_1.ingredients, vec!["trh", "fvjkl", "sbzzf", "mxmxvkd"]);
        assert_eq!(food_1.allergens, vec!["dairy"]);
    }

    #[test]
    fn test_example_1() {
        let foods: Vec<Food> = TEST_INPUT_1
            .iter()
            .map(|s| s.parse::<Food>().expect("Could not parse food."))
            .collect();

        let count = count_good(&foods);
        assert_eq!(count, 5);
    }

    #[test]
    fn test_example_2() {
        let foods: Vec<Food> = TEST_INPUT_1
            .iter()
            .map(|s| s.parse::<Food>().expect("Could not parse food."))
            .collect();

        let dangerous = find_dangerous(&foods);
        assert_eq!(dangerous.len(), 3);
        assert_eq!(dangerous, vec!["mxmxvkd", "sqjhc", "fvjkl"]);
    }
}
