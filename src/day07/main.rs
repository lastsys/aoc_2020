use common::input::fold_input;
use common::performance::timing;
use std::collections::hash_map::Entry;
use std::collections::{HashMap, HashSet};
use std::io::Error;

#[derive(Debug, PartialEq, Eq, Hash)]
struct Bag {
    tint: String,
    color: String,
}

#[derive(Debug)]
struct Rule {
    bag: Bag,
    content: Vec<(usize, Bag)>,
}

fn input_folder(container: &mut Vec<Rule>, line: Result<String, Error>) -> &mut Vec<Rule> {
    let rule = parse_rule(line.expect("Could not read line.").trim());
    container.push(rule);
    container
}

fn parse_bag(bag_rule: &str) -> Bag {
    let parts: Vec<&str> = bag_rule.split_whitespace().collect();
    Bag {
        tint: parts[0].to_string(),
        color: parts[1].to_string(),
    }
}

fn parse_rule(rule: &str) -> Rule {
    let top_level: Vec<&str> = rule.split("contain").map(|s| s.trim()).collect();
    let container = parse_bag(top_level[0]);

    let contents: Vec<&str> = top_level[1].split(',').map(|s| s.trim()).collect();

    let mut bag_content: Vec<(usize, Bag)> = Vec::new();
    for content in contents {
        match content {
            "no other bags." => (),
            x => {
                let parts: Vec<&str> = x.split_whitespace().map(|s| s.trim()).collect();
                let count: usize = parts[0].parse().expect("Could not parse number.");
                let bag_no_number: String = parts[1..3].join(" ");
                let bag = parse_bag(bag_no_number.as_str());
                bag_content.push((count, bag));
            }
        };
    }

    Rule {
        bag: container,
        content: bag_content,
    }
}

fn walk_bags(
    bag_tree: &HashMap<usize, HashSet<usize>>,
    bag_set: &mut HashSet<usize>,
    current_bag: usize,
) {
    if bag_tree[&current_bag].is_empty() {
        return;
    }
    for contained_bag in &bag_tree[&current_bag] {
        bag_set.insert(*contained_bag);
        walk_bags(bag_tree, bag_set, *contained_bag);
    }
}

fn count_bags(rules: &[Rule], search_bag: &Bag) -> usize {
    let mut bag_tree: HashMap<usize, HashSet<usize>> = HashMap::new();

    let mut bag_ids: HashMap<&Bag, usize> = HashMap::new();
    let mut bag_list: Vec<&Bag> = Vec::new();
    {
        // Give bags id:s.
        let mut id: usize = 0;
        for rule in rules {
            if !bag_ids.contains_key(&rule.bag) {
                bag_ids.insert(&rule.bag, id);
                bag_list.push(&rule.bag);
                id += 1;
            }
        }
    }

    // Initialize bag tree.
    for (i, _) in bag_list.iter().enumerate() {
        bag_tree.insert(i, HashSet::new());
    }
    // Reverse lookup.
    for rule in rules {
        for (_, contained_bag) in &rule.content {
            if bag_tree.contains_key(&bag_ids[contained_bag]) {
                let mut children = bag_tree[&bag_ids[&contained_bag]].clone();
                children.insert(bag_ids[&rule.bag]);
                bag_tree.insert(bag_ids[&contained_bag], children);
            }
        }
    }

    let mut bag_set: HashSet<usize> = HashSet::new();
    walk_bags(&bag_tree, &mut bag_set, bag_ids[search_bag]);

    bag_set.len()
}

fn walk_bags_2(
    bag_tree: &HashMap<usize, Vec<(usize, usize)>>,
    current_bag: usize,
    level: usize,
) -> usize {
    if bag_tree[&current_bag].is_empty() {
        0
    } else {
        let mut total_count = 0;
        for (bag_count, contained_bag) in &bag_tree[&current_bag] {
            total_count += bag_count;
            let child_count = walk_bags_2(bag_tree, *contained_bag, level + 1);
            total_count += bag_count * child_count;
        }
        total_count
    }
}

fn count_bags_2(rules: &[Rule], search_bag: &Bag) -> usize {
    let mut bag_tree: HashMap<usize, Vec<(usize, usize)>> = HashMap::new();

    let mut bag_ids: HashMap<&Bag, usize> = HashMap::new();
    let mut bag_list: Vec<&Bag> = Vec::new();
    {
        // Give bags id:s.
        let mut id: usize = 0;
        for rule in rules {
            if !bag_ids.contains_key(&rule.bag) {
                bag_ids.insert(&rule.bag, id);
                bag_list.push(&rule.bag);
                id += 1;
            }
        }
    }

    // Initialize bag tree.
    for (i, _) in bag_list.iter().enumerate() {
        bag_tree.insert(i, Vec::new());
    }
    for rule in rules {
        for (i, contained_bag) in &rule.content {
            match bag_tree.entry(bag_ids[&rule.bag]) {
                Entry::Vacant(e) => {
                    e.insert(vec![(*i, bag_ids[&contained_bag])]);
                }
                Entry::Occupied(mut e) => {
                    e.get_mut().push((*i, bag_ids[&contained_bag]));
                }
            }
        }
    }

    walk_bags_2(&bag_tree, bag_ids[search_bag], 0)
}

fn part1() {
    let mut init: Vec<Rule> = Vec::new();
    let rules =
        fold_input("input/day07.txt", &mut init, input_folder).expect("Could not read file.");

    let count = count_bags(
        rules,
        &Bag {
            tint: String::from("shiny"),
            color: String::from("gold"),
        },
    );

    println!("Bag count = {}", count);
}

fn part2() {
    let mut init: Vec<Rule> = Vec::new();
    let rules =
        fold_input("input/day07.txt", &mut init, input_folder).expect("Could not read file.");

    let count = count_bags_2(
        rules,
        &Bag {
            tint: String::from("shiny"),
            color: String::from("gold"),
        },
    );

    println!("Bag count = {}", count);
}

fn main() {
    timing(part1, 7, 1);
    timing(part2, 7, 2);
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_DATA: &[&str] = &[
        "light red bags contain 1 bright white bag, 2 muted yellow bags.",
        "dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
        "bright white bags contain 1 shiny gold bag.",
        "muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
        "shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
        "dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
        "vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
        "faded blue bags contain no other bags.",
        "dotted black bags contain no other bags.",
    ];

    #[test]
    fn test_parse_bag() {
        let bag_0 = parse_bag("light red bags");
        assert_eq!(bag_0.tint, "light");
        assert_eq!(bag_0.color, "red");
    }

    #[test]
    fn test_parse_rule() {
        let rule_0 = parse_rule(TEST_DATA[0]);
        assert_eq!(rule_0.bag.tint, String::from("light"));
        assert_eq!(rule_0.bag.color, String::from("red"));
        assert_eq!(rule_0.content.len(), 2);
        assert_eq!(rule_0.content[0].0, 1);
        assert_eq!(rule_0.content[0].1.tint, "bright");
        assert_eq!(rule_0.content[0].1.color, "white");
        assert_eq!(rule_0.content[1].0, 2);
        assert_eq!(rule_0.content[1].1.tint, "muted");
        assert_eq!(rule_0.content[1].1.color, "yellow");

        let rule_1 = parse_rule(TEST_DATA[1]);
        assert_eq!(rule_1.bag.tint, String::from("dark"));
        assert_eq!(rule_1.bag.color, String::from("orange"));
        assert_eq!(rule_1.content.len(), 2);
        assert_eq!(rule_1.content[0].0, 3);
        assert_eq!(rule_1.content[0].1.tint, "bright");
        assert_eq!(rule_1.content[0].1.color, "white");
        assert_eq!(rule_1.content[1].0, 4);
        assert_eq!(rule_1.content[1].1.tint, "muted");
        assert_eq!(rule_1.content[1].1.color, "yellow");

        let rule_2 = parse_rule(TEST_DATA[2]);
        assert_eq!(rule_2.bag.tint, String::from("bright"));
        assert_eq!(rule_2.bag.color, String::from("white"));
        assert_eq!(rule_2.content.len(), 1);
        assert_eq!(rule_2.content[0].0, 1);
        assert_eq!(rule_2.content[0].1.tint, "shiny");
        assert_eq!(rule_2.content[0].1.color, "gold");

        let rule_7 = parse_rule(TEST_DATA[7]);
        assert_eq!(rule_7.bag.tint, String::from("faded"));
        assert_eq!(rule_7.bag.color, String::from("blue"));
        assert_eq!(rule_7.content.len(), 0);
    }

    #[test]
    fn test_count_bags() {
        let rules: Vec<Rule> = TEST_DATA.iter().map(|s| parse_rule(s)).collect();
        let count = count_bags(
            &rules,
            &Bag {
                tint: String::from("shiny"),
                color: String::from("gold"),
            },
        );
        assert_eq!(count, 4);
    }

    #[test]
    fn test_count_bags_2() {
        let rules: Vec<Rule> = TEST_DATA.iter().map(|s| parse_rule(s)).collect();
        let count = count_bags_2(
            &rules,
            &Bag {
                tint: String::from("shiny"),
                color: String::from("gold"),
            },
        );
        assert_eq!(count, 32);
    }
}
