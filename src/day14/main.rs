use common::input::fold_input;
use common::performance::timing;
use std::collections::HashMap;
use std::io::Error;
use std::str::FromStr;

#[derive(Debug, PartialEq)]
enum Instruction {
    Mask([char; 36]),
    Mem(usize, u64),
}

struct Machine {
    pub memory: HashMap<usize, u64>,
    pub mask: [char; 36],
}

impl Machine {
    pub fn new() -> Machine {
        Machine {
            memory: HashMap::new(),
            mask: ['X'; 36],
        }
    }

    pub fn step(&mut self, instruction: &Instruction) {
        match instruction {
            Instruction::Mask(mask) => {
                self.mask = *mask;
            }
            Instruction::Mem(address, value) => {
                let mut v: u64 = *value;
                for i in 0usize..36usize {
                    match self.mask[35 - i] {
                        '1' => {
                            v |= 1 << i;
                        }
                        '0' => {
                            v &= !(1 << i);
                        }
                        _ => (),
                    }
                }
                self.memory.insert(*address, v);
            }
        }
    }

    pub fn step2(&mut self, instruction: &Instruction) {
        match instruction {
            Instruction::Mask(mask) => {
                self.mask = *mask;
            }
            Instruction::Mem(address, value) => {
                let v: u64 = *value;
                let mut addresses = vec![*address];
                for i in 0usize..36usize {
                    match self.mask[35 - i] {
                        '1' => {
                            let bits: usize = 1 << i;
                            for addr in &mut addresses {
                                *addr |= bits;
                            }
                        }
                        '0' => {}
                        _ => {
                            let bits: usize = 1 << i;
                            for addr in addresses.clone() {
                                addresses.push(addr ^ bits)
                            }
                        }
                    }
                }
                for addr in addresses {
                    self.memory.insert(addr, v);
                }
            }
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum InstructionErrorKind {
    SyntaxError,
}

#[derive(Clone, Debug, PartialEq, Eq)]
struct ParseInstructionError {
    kind: InstructionErrorKind,
}

impl FromStr for Instruction {
    type Err = ParseInstructionError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let pair: Vec<&str> = s.split(" = ").map(|s| s.trim()).collect();
        if pair.len() != 2 {
            Err(ParseInstructionError {
                kind: InstructionErrorKind::SyntaxError,
            })
        } else if pair[0].starts_with("mask") {
            let mut mask = ['X'; 36];
            for (i, bit) in pair[1].chars().enumerate() {
                mask[i] = bit;
            }
            Ok(Instruction::Mask(mask))
        } else if pair[0].starts_with("mem") {
            let address = &pair[0][4..(pair[0].len() - 1)]
                .parse::<usize>()
                .expect("Could not parse address.");
            let value = pair[1].parse::<u64>().expect("Could not parse value.");
            Ok(Instruction::Mem(*address, value))
        } else {
            Err(ParseInstructionError {
                kind: InstructionErrorKind::SyntaxError,
            })
        }
    }
}

fn input_folder(
    container: &mut Vec<Instruction>,
    line: Result<String, Error>,
) -> &mut Vec<Instruction> {
    container.push(
        line.expect("Could not read line.")
            .parse()
            .expect("Could not parse number."),
    );
    container
}

fn part1() {
    let mut init: Vec<Instruction> = Vec::new();
    let program: &Vec<Instruction> =
        fold_input("input/day14.txt", &mut init, input_folder).expect("Could not read input.");
    let mut machine = Machine::new();

    for instruction in program {
        machine.step(instruction);
    }

    println!("{}", machine.memory.values().sum::<u64>());
}

fn part2() {
    let mut init: Vec<Instruction> = Vec::new();
    let program: &Vec<Instruction> =
        fold_input("input/day14.txt", &mut init, input_folder).expect("Could not read input.");
    let mut machine = Machine::new();

    for instruction in program {
        machine.step2(instruction);
    }

    println!("{}", machine.memory.values().sum::<u64>());
}

fn main() {
    timing(part1, 14, 1);
    timing(part2, 14, 2);
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_DATA: &[&str] = &[
        "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X",
        "mem[8] = 11",
        "mem[7] = 101",
        "mem[8] = 0",
    ];

    const EXPANDED_MASK: &[char; 36] = &[
        'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X',
        'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', '1', 'X', 'X', 'X', 'X', '0', 'X',
    ];

    #[test]
    fn test_parse() {
        let i0: Instruction = TEST_DATA[0]
            .parse()
            .expect("Could not parse first instruction.");
        assert_eq!(i0, Instruction::Mask(*EXPANDED_MASK));

        let i1: Instruction = TEST_DATA[1]
            .parse()
            .expect("Could not parse second instruction.");
        assert_eq!(i1, Instruction::Mem(8, 11));

        let i2: Instruction = TEST_DATA[2]
            .parse()
            .expect("Could not parse third instruction.");
        assert_eq!(i2, Instruction::Mem(7, 101));

        let i3: Instruction = TEST_DATA[3]
            .parse()
            .expect("Could not parse fourth instruction.");
        assert_eq!(i3, Instruction::Mem(8, 0));
    }

    #[test]
    fn test_example_1() {
        let program: Vec<Instruction> = TEST_DATA
            .iter()
            .map(|s| {
                s.parse::<Instruction>()
                    .expect("Could not parse instruction.")
            })
            .collect();
        let mut machine = Machine::new();

        machine.step(&program[0]);
        assert_eq!(machine.memory.len(), 0);
        assert_eq!(machine.mask, *EXPANDED_MASK);

        machine.step(&program[1]);
        assert_eq!(machine.memory.len(), 1);
        assert_eq!(machine.memory[&8], 73);

        machine.step(&program[2]);
        assert_eq!(machine.memory.len(), 2);
        assert_eq!(machine.memory[&7], 101);

        machine.step(&program[3]);
        assert_eq!(machine.memory.len(), 2);
        assert_eq!(machine.memory[&8], 64);
    }
}
