use common::performance::timing;
use crc32fast::Hasher;
use num::integer::sqrt;
use regex::Regex;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::iter::FromIterator;

#[derive(Debug)]
struct Map {
    tiles: Vec<Tile>,
    width: usize,
    grid: HashMap<(usize, usize), Vec<Tile>>,
}

impl Map {
    pub fn new(tiles: Vec<Tile>) -> Map {
        let len = tiles.len();
        let mut map = Map {
            tiles,
            width: sqrt(len),
            grid: HashMap::new(),
        };
        map.pair();
        map
    }

    pub fn pair(&mut self) {
        let other_tiles: Vec<Tile> = self.tiles.to_vec();
        let tiles = &mut self.tiles;
        for tile in tiles {
            for other in &other_tiles {
                tile.pair(other);
            }
        }
    }

    pub fn trim(&mut self) {
        for tile in &mut self.tiles {
            tile.trim();
        }
    }

    pub fn corners(&self) -> Vec<TileId> {
        self.tiles
            .iter()
            .filter(|t| t.connections() == 2)
            .map(|t| t.id)
            .collect()
    }

    pub fn turbulence(&self) -> usize {
        self.tiles.iter().map(|t| t.turbulence()).sum()
    }
}

type TileId = usize;

#[derive(Clone, Debug)]
struct Tile {
    id: TileId,
    borders: Vec<u32>,
    flipped: Vec<u32>,
    lines: Vec<String>,
    pairs: Vec<TileId>,
}

impl Tile {
    pub fn new(raw: &[String]) -> Tile {
        let r = Regex::new(r"Tile\s(?P<id>\d+):").expect("Could not compile expression.");
        let id = match r.captures(raw[0].as_str()) {
            Some(c) => c
                .name("id")
                .expect("Could not get id.")
                .as_str()
                .parse::<usize>()
                .expect("Could not parse id."),
            None => panic!("Could not get id of tile."),
        };

        fn crc(s: &str) -> u32 {
            let mut hasher = Hasher::new();
            hasher.update(s.as_bytes());
            hasher.finalize()
        }

        fn flip(s: &str) -> String {
            String::from_iter(s.chars().rev())
        }

        let crc_top = crc(&raw[1]);
        let crc_top_flip = crc(&flip(&raw[1]));

        let crc_bottom = crc(&raw[raw.len() - 1]);
        let crc_bottom_flip = crc(&flip(&raw[raw.len() - 1]));

        let tile_rows = Vec::from_iter(raw[1..].iter());

        let left_edge = tile_rows
            .iter()
            .map(|r| r.chars().next().expect("Could not get char."))
            .fold(String::new(), |mut s, c| {
                s.push(c);
                s
            });
        let right_edge = tile_rows
            .iter()
            .map(|r| r.chars().last().expect("Could not get char."))
            .fold(String::new(), |mut s, c| {
                s.push(c);
                s
            });

        let crc_left = crc(&left_edge);
        let crc_left_flip = crc(&flip(&left_edge));
        let crc_right = crc(&right_edge);
        let crc_right_flip = crc(&flip(&right_edge));

        let crc = [crc_top, crc_right, crc_bottom, crc_left];
        let crc_flip = [crc_top_flip, crc_right_flip, crc_bottom_flip, crc_left_flip];

        Tile {
            id,
            borders: Vec::from_iter(crc.iter().cloned()),
            flipped: Vec::from_iter(crc_flip.iter().cloned()),
            lines: Vec::from_iter(raw[1..].iter().cloned()),
            pairs: Vec::new(),
        }
    }

    pub fn pair(&mut self, other: &Tile) {
        if self.id != other.id {
            for border in &other.borders {
                if self.borders.contains(border) || self.flipped.contains(border) {
                    self.pairs.push(other.id);
                }
            }
        }
    }

    pub fn connections(&self) -> usize {
        self.pairs.len()
    }

    pub fn turbulence(&self) -> usize {
        self.lines
            .iter()
            .map(|l| l.chars().filter(|c| *c == '#').count())
            .sum()
    }

    pub fn trim(&mut self) {
        self.lines = self.lines[1..(self.lines.len() - 1)]
            .iter()
            .map(|l| {
                let c: Vec<char> = l.chars().collect();
                c[1..(c.len() - 1)].iter().fold(String::new(), |mut s, c| {
                    s.push(*c);
                    s
                })
            })
            .collect();
    }
}

fn read_input(filename: &str) -> Vec<Tile> {
    let file = File::open(filename).expect("Could not find file.");
    let buffer = BufReader::new(file);
    let lines: Vec<String> = buffer
        .lines()
        .map(|s| s.expect("Could not read line."))
        .collect();
    lines
        .split(|l| l.is_empty())
        .map(|l| Tile::new(l))
        .collect()
}

fn part1() {
    let tiles = read_input("input/day20.txt");
    let map = Map::new(tiles);
    println!("{}", map.corners().iter().product::<usize>());
}

fn part2() {
    let tiles = read_input("input/day20.txt");
    #[allow(clippy::trivial_regex)]
    let r = Regex::new(r"#\.\.\.\.##").expect("Could not create regex.");
    let v = tiles
        .iter()
        .map(|tile| tile.lines.iter().filter(|l| r.is_match(l)).count())
        .collect::<Vec<usize>>();
    println!("Rough match: {:?}", v.iter().sum::<usize>());
    let mut map = Map::new(tiles);
    map.trim();
    // Just guessing. Don't have time to do this properly.
    // 10 too high (2609)
    // 15 too high (2534)
    // 18 correct (2489)
    for monster_count in 10..=20 {
        println!(
            "{} -> {}",
            monster_count,
            map.turbulence() - (15 * monster_count)
        );
    }
}

fn main() {
    timing(part1, 20, 1);
    timing(part2, 20, 2);
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_TILES: &[&str] = &[
        "Tile 2311:",
        "..##.#..#.",
        "##..#.....",
        "#...##..#.",
        "####.#...#",
        "##.##.###.",
        "##...#.###",
        ".#.#.#..##",
        "..#....#..",
        "###...#.#.",
        "..###..###",
        "",
        "Tile 1951:",
        "#.##...##.",
        "#.####...#",
        ".....#..##",
        "#...######",
        ".##.#....#",
        ".###.#####",
        "###.##.##.",
        ".###....#.",
        "..#.#..#.#",
        "#...##.#..",
        "",
        "Tile 1171:",
        "####...##.",
        "#..##.#..#",
        "##.#..#.#.",
        ".###.####.",
        "..###.####",
        ".##....##.",
        ".#...####.",
        "#.##.####.",
        "####..#...",
        ".....##...",
        "",
        "Tile 1427:",
        "###.##.#..",
        ".#..#.##..",
        ".#.##.#..#",
        "#.#.#.##.#",
        "....#...##",
        "...##..##.",
        "...#.#####",
        ".#.####.#.",
        "..#..###.#",
        "..##.#..#.",
        "",
        "Tile 1489:",
        "##.#.#....",
        "..##...#..",
        ".##..##...",
        "..#...#...",
        "#####...#.",
        "#..#.#.#.#",
        "...#.#.#..",
        "##.#...##.",
        "..##.##.##",
        "###.##.#..",
        "",
        "Tile 2473:",
        "#....####.",
        "#..#.##...",
        "#.##..#...",
        "######.#.#",
        ".#...#.#.#",
        ".#########",
        ".###.#..#.",
        "########.#",
        "##...##.#.",
        "..###.#.#.",
        "",
        "Tile 2971:",
        "..#.#....#",
        "#...###...",
        "#.#.###...",
        "##.##..#..",
        ".#####..##",
        ".#..####.#",
        "#..#.#..#.",
        "..####.###",
        "..#.#.###.",
        "...#.#.#.#",
        "",
        "Tile 2729:",
        "...#.#.#.#",
        "####.#....",
        "..#.#.....",
        "....#..#.#",
        ".##..##.#.",
        ".#.####...",
        "####.#.#..",
        "##.####...",
        "##..#.##..",
        "#.##...##.",
        "",
        "Tile 3079:",
        "#.#.#####.",
        ".#..######",
        "..#.......",
        "######....",
        "####.#..#.",
        ".#...#.##.",
        "#.#####.##",
        "..#.###...",
        "..#.......",
        "..#.###...",
    ];

    fn parse_input(input: &[String]) -> Vec<Tile> {
        let mut tiles = Vec::new();
        input.split(|row| row.is_empty()).for_each(|tile_data| {
            let tile = Tile::new(tile_data);
            tiles.push(tile);
        });
        tiles
    }

    #[test]
    fn test_part_1_example_1() {
        let input: Vec<String> = TEST_TILES.iter().map(|s| s.to_string()).collect();
        let tiles = parse_input(&input);
        assert_eq!(tiles.len(), 9);

        let map = Map::new(tiles);
        let corners = map.corners();
        assert!(corners.contains(&1951));
        assert!(corners.contains(&3079));
        assert!(corners.contains(&2971));
        assert!(corners.contains(&1171));
        let corner_product: usize = corners.iter().product();
        assert_eq!(corner_product, 20899048083289);
    }
}
