extern crate pest;
#[macro_use]
extern crate pest_derive;

use common::input::fold_input;
use common::performance::timing;
use pest::Parser;
use std::io::Error;

#[derive(Parser)]
#[grammar = "day02/day02.pest"]
struct PasswordParser;

#[derive(Debug)]
struct Password {
    pub minimum: usize,
    pub maximum: usize,
    pub character: char,
    pub password: String,
}

fn parse_line(line: &str) -> Password {
    let mut parsed = PasswordParser::parse(Rule::line, line)
        .expect("Failed to parse line.")
        .next()
        .expect("No next parse result.")
        .into_inner();

    Password {
        minimum: parsed
            .next()
            .expect("Could not get minimum field.")
            .as_str()
            .parse()
            .expect("Could not parse minimum value."),
        maximum: parsed
            .next()
            .expect("Could not get maximum field.")
            .as_str()
            .parse()
            .expect("Could not parse maximum value."),
        character: parsed
            .next()
            .expect("Could not get character field.")
            .as_str()
            .chars()
            .next()
            .expect("Could not get character."),
        password: parsed
            .next()
            .expect("Could not get password field.")
            .as_str()
            .to_string(),
    }
}

fn input_folder(container: &mut Vec<Password>, line: Result<String, Error>) -> &mut Vec<Password> {
    let current_line = line.expect("Failed to read line.");
    let password = parse_line(current_line.as_str());
    container.push(password);
    container
}

fn check_password(password: &Password) -> bool {
    let mut count: usize = 0;
    for char in password.password.chars() {
        if char == password.character {
            count += 1;
        }
    }
    count >= password.minimum && count <= password.maximum
}

fn part1() {
    let mut init: Vec<Password> = Vec::new();
    let inputs: &Vec<Password> =
        fold_input("input/day02.txt", &mut init, input_folder).expect("Failed to read input.");
    let mut valid_count: usize = 0;
    for input in inputs {
        if check_password(input) {
            valid_count += 1;
        }
    }
    println!("Valid passwords = {}", valid_count);
}

fn check_password_2(password: &Password) -> bool {
    let chars: Vec<char> = password.password.chars().collect();
    let first = chars[password.minimum - 1] == password.character;
    let second = chars[password.maximum - 1] == password.character;
    first != second
}

fn part2() {
    let mut init: Vec<Password> = Vec::new();
    let inputs: &Vec<Password> =
        fold_input("input/day02.txt", &mut init, input_folder).expect("Failed to read input.");
    let mut valid_count: usize = 0;
    for input in inputs {
        if check_password_2(input) {
            valid_count += 1;
        }
    }
    println!("Valid passwords = {}", valid_count);
}

fn main() {
    timing(part1, 2, 1);
    timing(part2, 2, 2);
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &'static [&str] = &["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"];

    #[test]
    fn test_parser() {
        let p0 = parse_line(TEST_INPUT[0]);
        assert_eq!(p0.minimum, 1);
        assert_eq!(p0.maximum, 3);
        assert_eq!(p0.character, 'a');
        assert_eq!(p0.password, "abcde");

        let p1 = parse_line(TEST_INPUT[1]);
        assert_eq!(p1.minimum, 1);
        assert_eq!(p1.maximum, 3);
        assert_eq!(p1.character, 'b');
        assert_eq!(p1.password, "cdefg");

        let p2 = parse_line(TEST_INPUT[2]);
        assert_eq!(p2.minimum, 2);
        assert_eq!(p2.maximum, 9);
        assert_eq!(p2.character, 'c');
        assert_eq!(p2.password, "ccccccccc");
    }

    #[test]
    fn test_password_checker() {
        let p0 = parse_line(TEST_INPUT[0]);
        assert_eq!(check_password(&p0), true);
        let p1 = parse_line(TEST_INPUT[1]);
        assert_eq!(check_password(&p1), false);
        let p2 = parse_line(TEST_INPUT[2]);
        assert_eq!(check_password(&p2), true);
    }

    #[test]
    fn test_second_password_checker() {
        let p0 = parse_line(TEST_INPUT[0]);
        assert_eq!(check_password_2(&p0), true);
        let p1 = parse_line(TEST_INPUT[1]);
        assert_eq!(check_password_2(&p1), false);
        let p2 = parse_line(TEST_INPUT[2]);
        assert_eq!(check_password_2(&p2), false);
    }
}
